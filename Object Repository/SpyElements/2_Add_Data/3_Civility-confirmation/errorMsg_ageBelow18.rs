<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMsg_ageBelow18</name>
   <tag></tag>
   <elementGuidId>f637ef08-26a2-4b9a-9efb-c070ba359d32</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Vous devez avoir plus de 18 ans' or . = 'Vous devez avoir plus de 18 ans')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Vous devez avoir plus de 18 ans</value>
   </webElementProperties>
</WebElementEntity>
