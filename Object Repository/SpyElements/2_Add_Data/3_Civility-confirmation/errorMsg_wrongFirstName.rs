<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMsg_wrongFirstName</name>
   <tag></tag>
   <elementGuidId>f54a3611-0d37-4bff-ad65-3ebe1df89db8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Votre prénom ne peut pas contenir de chiffres ou de caractères spéciaux' or . = 'Votre prénom ne peut pas contenir de chiffres ou de caractères spéciaux')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Votre prénom ne peut pas contenir de chiffres ou de caractères spéciaux</value>
   </webElementProperties>
</WebElementEntity>
