<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMsg_ageAbove120</name>
   <tag></tag>
   <elementGuidId>600365f8-0de6-4904-95df-01fe5c23dee0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Vous ne pouvez pas avoir plus de 120 ans' or . = 'Vous ne pouvez pas avoir plus de 120 ans')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Vous ne pouvez pas avoir plus de 120 ans</value>
   </webElementProperties>
</WebElementEntity>
