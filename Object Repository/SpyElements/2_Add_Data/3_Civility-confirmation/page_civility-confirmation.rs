<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_civility-confirmation</name>
   <tag></tag>
   <elementGuidId>fbcfa8cc-07c3-4ead-a92f-7d8d9dbf3b9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Confirmez vos informations' or . = 'Confirmez vos informations')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Confirmez vos informations</value>
   </webElementProperties>
</WebElementEntity>
