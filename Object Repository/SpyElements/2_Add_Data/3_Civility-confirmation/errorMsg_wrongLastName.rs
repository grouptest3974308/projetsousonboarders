<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMsg_wrongLastName</name>
   <tag></tag>
   <elementGuidId>f71e4180-d617-4407-8d59-98c07c6ed86b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Votre nom ne peut pas contenir de chiffres ou de caractères spéciaux' or . = 'Votre nom ne peut pas contenir de chiffres ou de caractères spéciaux')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Votre nom ne peut pas contenir de chiffres ou de caractères spéciaux</value>
   </webElementProperties>
</WebElementEntity>
