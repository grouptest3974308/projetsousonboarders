<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_private-life-certification</name>
   <tag></tag>
   <elementGuidId>fc8f5925-255c-46c7-8287-9b1a98804ab7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Confirmez vos informations' or . = 'Confirmez vos informations')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Confirmez vos informations</value>
   </webElementProperties>
</WebElementEntity>
