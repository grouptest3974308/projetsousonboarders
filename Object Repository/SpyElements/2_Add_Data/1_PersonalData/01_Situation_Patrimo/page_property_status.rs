<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_property_status</name>
   <tag></tag>
   <elementGuidId>93d323f3-8750-49fb-a0de-f01100f04af4</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'Quelle est votre situation de logement') or contains(., 'Quelle est votre situation de logement'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Quelle est votre situation de logement</value>
   </webElementProperties>
</WebElementEntity>
