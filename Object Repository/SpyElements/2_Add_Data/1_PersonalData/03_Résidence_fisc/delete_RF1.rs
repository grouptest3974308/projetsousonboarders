<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>delete_RF1</name>
   <tag></tag>
   <elementGuidId>9728424a-54b8-4bc4-a148-a3964c79556e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-testid = 'taxResidenceCountries#SelectedItem#0#DeleteButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>taxResidenceCountries#SelectedItem#0#DeleteButton</value>
   </webElementProperties>
</WebElementEntity>
