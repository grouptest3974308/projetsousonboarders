<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>popup_numIDFisc_a_fournir</name>
   <tag></tag>
   <elementGuidId>6734ed5d-ba57-42d3-9057-ec9137fa0bd3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = concat('Numéros d' , &quot;'&quot; , 'Identification Fiscale à fournir') or . = concat('Numéros d' , &quot;'&quot; , 'Identification Fiscale à fournir'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Numéros d'Identification Fiscale à fournir</value>
   </webElementProperties>
</WebElementEntity>
