<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMessage_PaysHorsUEAELE</name>
   <tag></tag>
   <elementGuidId>88b76c1f-d6bb-403b-8455-8f19f47eeaab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.sc-QxirK.fWxumN</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Malheureusement, Nickel n’est pas disponible pour les clients ayant des résidences fiscales hors UE ou AELE. Pays membres de l’UE ou de l’AELE :  Allemagne, Autriche, Belgique, Bulgarie, Chypre, Croatie, Danemark, Espagne, Estonie, Finlande, France, Grèce, Hongrie, Irlande, Islande, Italie, Lettonie, Liechtenstein, Lituanie, Luxembourg, Malte, Norvège, Pays-Bas, Pologne, Portugal, République Tchèque, Roumanie, Royaume-Uni, Slovaquie, Slovénie, Suède, Suisse, Ukraine' or . = 'Malheureusement, Nickel n’est pas disponible pour les clients ayant des résidences fiscales hors UE ou AELE. Pays membres de l’UE ou de l’AELE :  Allemagne, Autriche, Belgique, Bulgarie, Chypre, Croatie, Danemark, Espagne, Estonie, Finlande, France, Grèce, Hongrie, Irlande, Islande, Italie, Lettonie, Liechtenstein, Lituanie, Luxembourg, Malte, Norvège, Pays-Bas, Pologne, Portugal, République Tchèque, Roumanie, Royaume-Uni, Slovaquie, Slovénie, Suède, Suisse, Ukraine')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>433ae81e-fc00-4589-b5a0-b89dbde46f9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sc-QxirK fWxumN</value>
      <webElementGuid>4520988c-26cd-46f8-b63a-f2d69fe1452e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Malheureusement, Nickel n’est pas disponible pour les clients ayant des résidences fiscales hors UE ou AELE. Pays membres de l’UE ou de l’AELE :  Allemagne, Autriche, Belgique, Bulgarie, Chypre, Croatie, Danemark, Espagne, Estonie, Finlande, France, Grèce, Hongrie, Irlande, Islande, Italie, Lettonie, Liechtenstein, Lituanie, Luxembourg, Malte, Norvège, Pays-Bas, Pologne, Portugal, République Tchèque, Roumanie, Royaume-Uni, Slovaquie, Slovénie, Suède, Suisse, Ukraine</value>
      <webElementGuid>b2abc9e8-6319-4f82-bcd1-4e46a3c49389</webElementGuid>
   </webElementProperties>
</WebElementEntity>
