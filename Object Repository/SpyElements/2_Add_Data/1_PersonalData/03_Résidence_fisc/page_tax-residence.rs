<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_tax-residence</name>
   <tag></tag>
   <elementGuidId>86f4bcd5-e9c4-4f96-925c-32ca821c2329</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'Êtes-vous résident fiscal uniquement en France') or contains(., 'Êtes-vous résident fiscal uniquement en France'))]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>IMAGE</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Êtes-vous résident fiscal uniquement en France</value>
      <webElementGuid>3a837868-3af1-4bae-a6ec-5fb1134b48be</webElementGuid>
   </webElementProperties>
</WebElementEntity>
