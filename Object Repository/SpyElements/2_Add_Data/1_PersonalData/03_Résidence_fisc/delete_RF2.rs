<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>delete_RF2</name>
   <tag></tag>
   <elementGuidId>f8086dfd-6874-4a58-8d31-ce0e834f585b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-testid = 'taxResidenceCountries#SelectedItem#1#DeleteButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>taxResidenceCountries#SelectedItem#1#DeleteButton</value>
   </webElementProperties>
</WebElementEntity>
