<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_us-person</name>
   <tag></tag>
   <elementGuidId>80d2c41c-3f8a-44ef-a38d-801d18781ca7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'Êtes-vous citoyen américain, résident américain ou né aux Etats-Unis') or contains(., 'Êtes-vous citoyen américain, résident américain ou né aux Etats-Unis'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Êtes-vous citoyen américain, résident américain ou né aux Etats-Unis</value>
   </webElementProperties>
</WebElementEntity>
