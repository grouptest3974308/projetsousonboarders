<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Veuillez saisir votre nouvelle adresse email</name>
   <tag></tag>
   <elementGuidId>83daa2f0-c9b9-428a-8439-765bd6606813</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Veuillez saisir votre nouvelle adresse email :' or . = 'Veuillez saisir votre nouvelle adresse email :')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Veuillez saisir votre nouvelle adresse email :</value>
   </webElementProperties>
</WebElementEntity>
