<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_additionalInfo</name>
   <tag></tag>
   <elementGuidId>b83de611-74b3-4fea-a7ae-0495a30e4098</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@data-testid = 'additionalInfo#Input']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2ede3799-10b2-448f-881f-b46dde71c7f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>additionalInfo#Input</value>
      <webElementGuid>c33d6682-58a3-4262-810a-e03b5707fcbc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
