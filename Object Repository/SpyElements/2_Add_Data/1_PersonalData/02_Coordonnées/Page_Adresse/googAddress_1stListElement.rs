<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>googAddress_1stListElement</name>
   <tag></tag>
   <elementGuidId>424b0e64-15d1-4c79-b6df-d301148a0901</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-testid = 'address#select#option#0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>address#select#option#0</value>
      <webElementGuid>f3f8a361-17aa-4e79-a210-519c3821dc61</webElementGuid>
   </webElementProperties>
</WebElementEntity>
