<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_adresse manuellement</name>
   <tag></tag>
   <elementGuidId>4b315437-7468-4c0d-8ec2-c3b9010ed36d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.sc-gtczok.icTXpP.sc-jSpqJA.sc-qdQaH.ertjGv.bouCTm.sc-kEbeIB.dWamZt > button[type=&quot;button&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-testid = 'ManualEntryButton']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0c6923eb-8f6f-4e27-ac69-9705b75549ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>ManualEntryButton</value>
      <webElementGuid>8bb09481-2ffd-4a49-bd5c-50ee3e614607</webElementGuid>
   </webElementProperties>
</WebElementEntity>
