<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Adresse postale</name>
   <tag></tag>
   <elementGuidId>38d0f50f-d2c0-4f6d-9d83-dd0e6ef486ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-testid = 'address#ValueDisplay']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>address#ValueDisplay</value>
   </webElementProperties>
</WebElementEntity>
