<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_addressDropdownButton</name>
   <tag></tag>
   <elementGuidId>e947285c-8edb-4e84-a47e-ac8e03541bd8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-testid = 'address#ToggleDropdownButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>address#ToggleDropdownButton</value>
      <webElementGuid>302e4a73-4bf2-4dcf-9421-bc48ca174506</webElementGuid>
   </webElementProperties>
</WebElementEntity>
