<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_birth-data-confirmation</name>
   <tag></tag>
   <elementGuidId>357375be-dc82-4868-a9d9-b0bb5e413351</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Confirmez vos informations' or . = 'Confirmez vos informations')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Confirmez vos informations</value>
   </webElementProperties>
</WebElementEntity>
