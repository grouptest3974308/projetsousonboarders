<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>error_msg_floue</name>
   <tag></tag>
   <elementGuidId>1058796c-e40e-4e0d-9da0-512bfc95c680</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'L’image est floue' or . = 'L’image est floue')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>L’image est floue</value>
      <webElementGuid>559a68eb-5b22-45df-acba-66562725aaf4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
