<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_identity_liveness</name>
   <tag></tag>
   <elementGuidId>64c65f61-50dc-4a86-b051-75b61f79b943</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Sélectionnez un document' or . = 'Sélectionnez un document')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sélectionnez un document</value>
      <webElementGuid>9abf474d-27fb-46e0-be84-06fc67a13d67</webElementGuid>
   </webElementProperties>
</WebElementEntity>
