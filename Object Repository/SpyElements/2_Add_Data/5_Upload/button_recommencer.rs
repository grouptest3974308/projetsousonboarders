<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_recommencer</name>
   <tag></tag>
   <elementGuidId>cef13711-86c0-4331-92db-4e9a75617d78</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@data-onfido-qa = 'redo-action-btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2cd704de-2dee-4909-ba8b-d83a4a860104</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-onfido-qa</name>
      <type>Main</type>
      <value>redo-action-btn</value>
      <webElementGuid>8e667fd2-dbd0-46b2-91b2-482180882679</webElementGuid>
   </webElementProperties>
</WebElementEntity>
