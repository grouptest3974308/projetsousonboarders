<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_add_Resident_Card_recto</name>
   <tag></tag>
   <elementGuidId>a8f915ff-cc85-4708-ae0e-b196925d6048</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Envoyer la carte de séjour (recto)' or . = 'Envoyer la carte de séjour (recto)')]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Envoyer la carte de séjour (recto)</value>
      <webElementGuid>97866be7-8299-4dfe-b606-ed220c8ca4e7</webElementGuid>
   </webElementProperties>
</WebElementEntity>
