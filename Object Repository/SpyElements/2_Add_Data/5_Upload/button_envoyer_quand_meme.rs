<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_envoyer_quand_meme</name>
   <tag></tag>
   <elementGuidId>94c0ff57-ad78-4d7c-9bd0-32ee754e90b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@data-onfido-qa = 'confirm-action-btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>01493dfe-54ec-4c8e-b335-c048948452d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-onfido-qa</name>
      <type>Main</type>
      <value>confirm-action-btn</value>
      <webElementGuid>57ca3869-1030-4f37-9e46-1b1f22c59fe1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
