<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_verifiez_votre_image</name>
   <tag></tag>
   <elementGuidId>3e800711-28b3-4203-a9ca-d69b1aa00923</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Vérifiez votre image' or . = 'Vérifiez votre image')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Vérifiez votre image</value>
      <webElementGuid>52ff4960-42ea-4be8-afdc-6a42d4a14483</webElementGuid>
   </webElementProperties>
</WebElementEntity>
