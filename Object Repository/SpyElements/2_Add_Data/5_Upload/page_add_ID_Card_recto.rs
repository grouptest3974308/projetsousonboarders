<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_add_ID_Card_recto</name>
   <tag></tag>
   <elementGuidId>020ddca4-5118-400b-b6e9-da213c37dd0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Envoyer la carte d’identité (recto)' or . = 'Envoyer la carte d’identité (recto)')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1.sc-dkyMPD.boHIn</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Envoyer la carte d’identité (recto)</value>
      <webElementGuid>80911f83-2fed-4337-b946-f09f05c231ca</webElementGuid>
   </webElementProperties>
</WebElementEntity>
