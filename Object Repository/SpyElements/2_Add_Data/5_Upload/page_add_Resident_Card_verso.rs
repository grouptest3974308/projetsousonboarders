<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_add_Resident_Card_verso</name>
   <tag></tag>
   <elementGuidId>2c6fffdf-b86f-409e-b44a-446b49674702</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Envoyer la carte de séjour (verso)' or . = 'Envoyer la carte de séjour (verso)')]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Envoyer la carte de séjour (verso)</value>
      <webElementGuid>0bfb7764-ebe5-4588-a52a-78dc3f30e7ae</webElementGuid>
   </webElementProperties>
</WebElementEntity>
