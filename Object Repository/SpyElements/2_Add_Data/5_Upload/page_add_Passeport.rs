<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_add_Passeport</name>
   <tag></tag>
   <elementGuidId>eb5daeab-edbb-4238-a8d7-8fd87e4da1f0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Importez la page du passeport sur laquelle figure votre photo' or . = 'Importez la page du passeport sur laquelle figure votre photo')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Importez la page du passeport sur laquelle figure votre photo</value>
      <webElementGuid>25533ffa-bc75-4749-aff3-9ca74a4aa01c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
