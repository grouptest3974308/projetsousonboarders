<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>error_msg_reflet</name>
   <tag></tag>
   <elementGuidId>7dc815fb-c549-44ab-a5b8-5bdbd9e4ae64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Attention aux reflets' or . = 'Attention aux reflets')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Attention aux reflets</value>
      <webElementGuid>c912fc22-80c0-421c-806e-6a86181aba9d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
