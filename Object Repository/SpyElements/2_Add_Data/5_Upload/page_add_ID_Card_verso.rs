<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_add_ID_Card_verso</name>
   <tag></tag>
   <elementGuidId>a9a8ad0f-6ca9-427f-9142-6d4c469ac63a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.sc-dkyMPD.boHIn</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Envoyez votre carte d’identité (verso)' or . = 'Envoyez votre carte d’identité (verso)')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Envoyez votre carte d’identité (verso)</value>
      <webElementGuid>a5b11d37-6304-449c-809d-47f9fbf54bb1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
