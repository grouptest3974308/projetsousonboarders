<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_envoyez_photo</name>
   <tag></tag>
   <elementGuidId>bd1e86d8-694e-4ab2-8a87-261bb7160552</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@data-onfido-qa = 'uploaderButtonLink']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>950652be-8cd5-4ed8-b917-1239c97c6fb8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-onfido-qa</name>
      <type>Main</type>
      <value>uploaderButtonLink</value>
      <webElementGuid>d20991d4-991d-4103-86d6-6d32a1d41d58</webElementGuid>
   </webElementProperties>
</WebElementEntity>
