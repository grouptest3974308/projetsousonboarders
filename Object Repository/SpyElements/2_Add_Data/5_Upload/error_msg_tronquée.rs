<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>error_msg_tronquée</name>
   <tag></tag>
   <elementGuidId>2f188fd8-a2dc-4a66-86d3-191439615896</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Image tronquée détectée' or . = 'Image tronquée détectée')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Image tronquée détectée</value>
      <webElementGuid>5183b0ea-4767-499c-9314-4ca7772fa02f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
