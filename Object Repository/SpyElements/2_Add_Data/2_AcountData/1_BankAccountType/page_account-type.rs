<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_account-type</name>
   <tag></tag>
   <elementGuidId>30c16ff5-dc4f-458c-a78c-c805877eae78</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'Est-ce que Nickel sera votre compte bancaire principal') or contains(., 'Est-ce que Nickel sera votre compte bancaire principal'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Est-ce que Nickel sera votre compte bancaire principal</value>
   </webElementProperties>
</WebElementEntity>
