<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>07_Sans revenus réguliers</name>
   <tag></tag>
   <elementGuidId>c05b9a76-aaf0-46cb-9d98-8c2c7092c350</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Sans revenus réguliers' or . = 'Sans revenus réguliers')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sans revenus réguliers</value>
   </webElementProperties>
</WebElementEntity>
