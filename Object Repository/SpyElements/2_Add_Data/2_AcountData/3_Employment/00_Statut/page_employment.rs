<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_employment</name>
   <tag></tag>
   <elementGuidId>fcec8296-869c-4230-aad1-ab39aa5bd801</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Quelle est votre situation professionnelle ?' or . = 'Quelle est votre situation professionnelle ?')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Quelle est votre situation professionnelle ?</value>
   </webElementProperties>
</WebElementEntity>
