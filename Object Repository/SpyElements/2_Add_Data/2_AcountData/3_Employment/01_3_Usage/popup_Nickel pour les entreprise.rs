<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>popup_Nickel pour les entreprise</name>
   <tag></tag>
   <elementGuidId>337db986-1a85-4fd3-9eb8-04a259356d2f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Nickel, ce n’est pas pour les entreprises' or . = 'Nickel, ce n’est pas pour les entreprises')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Nickel, ce n’est pas pour les entreprises</value>
   </webElementProperties>
</WebElementEntity>
