<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Annuler louverture de mon compte</name>
   <tag></tag>
   <elementGuidId>8703f184-247c-4d54-9359-0d0e414f5ff0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-testid = 'ModalSecondaryActionButton']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Annuler l'ouverture de mon compte</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>ModalSecondaryActionButton</value>
   </webElementProperties>
</WebElementEntity>
