<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Ouvrir_cmp_usage priv</name>
   <tag></tag>
   <elementGuidId>5fc2e2b7-d895-467b-b566-6b68ab99c649</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@data-testid = 'ModalPrimaryActionButton']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button[type=&quot;button&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>ModalPrimaryActionButton</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ouvrir un compte Nickel à usage privé</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/form[1]/div[@class=&quot;sc-jlAoix KeReJ&quot;]/div[@class=&quot;sc-hKyzHW jXVqwA&quot;]/div[@class=&quot;sc-gtczok bjKqqp sc-hKVsly cQqCfy sc-kEbeIB dWamZt&quot;]/button[1]</value>
   </webElementProperties>
</WebElementEntity>
