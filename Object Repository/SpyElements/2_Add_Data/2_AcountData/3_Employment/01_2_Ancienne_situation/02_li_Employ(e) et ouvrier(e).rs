<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>02_li_Employ(e) et ouvrier(e)</name>
   <tag></tag>
   <elementGuidId>11e3a43b-626f-49b7-a1f8-d4160d4b778a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Employé(e) et ouvrier(e)' or . = 'Employé(e) et ouvrier(e)')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#downshift-23-item-2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>0e6c53d3-f64d-4686-aae9-d8e919a71905</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Employé(e) et ouvrier(e)</value>
      <webElementGuid>d8c8ce03-1349-4471-8ba7-d1dec81fffd8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
