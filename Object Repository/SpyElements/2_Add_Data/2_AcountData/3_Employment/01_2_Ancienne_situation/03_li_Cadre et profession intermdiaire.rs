<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>03_li_Cadre et profession intermdiaire</name>
   <tag></tag>
   <elementGuidId>c21b2f80-99bc-4d06-9652-86ef1944fba2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Cadre et profession intermédiaire' or . = 'Cadre et profession intermédiaire')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#downshift-23-item-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>13d220e3-3fe4-4cd9-b391-2db6cd2c1434</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cadre et profession intermédiaire</value>
      <webElementGuid>175febdc-e64a-41d3-9bad-4694a1ef9f43</webElementGuid>
   </webElementProperties>
</WebElementEntity>
