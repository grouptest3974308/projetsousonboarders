<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_account-use</name>
   <tag></tag>
   <elementGuidId>926edcdb-7dad-4f57-83f7-a235735d698c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'À quoi va servir votre compte') or contains(., 'À quoi va servir votre compte'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>À quoi va servir votre compte</value>
   </webElementProperties>
</WebElementEntity>
