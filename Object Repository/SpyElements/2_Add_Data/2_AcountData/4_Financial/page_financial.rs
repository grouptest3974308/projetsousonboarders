<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_financial</name>
   <tag></tag>
   <elementGuidId>b20143e1-9a40-4d58-a076-3fc58c9bc575</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'De quelles ressources disposez-vous') or contains(., 'De quelles ressources disposez-vous'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>De quelles ressources disposez-vous</value>
   </webElementProperties>
</WebElementEntity>
