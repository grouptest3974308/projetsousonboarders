<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>00_li_- de 500</name>
   <tag></tag>
   <elementGuidId>de07ef01-6aaf-49b3-b7c2-c2b73f120cd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '- de 500€' or . = '- de 500€')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- de 500€</value>
   </webElementProperties>
</WebElementEntity>
