<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_de 1 000 000</name>
   <tag></tag>
   <elementGuidId>b5ca48eb-4227-4b9e-a7fc-6a94b1ff64b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '+ de 1 000 000€' or . = '+ de 1 000 000€')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>+ de 1 000 000€</value>
   </webElementProperties>
</WebElementEntity>
