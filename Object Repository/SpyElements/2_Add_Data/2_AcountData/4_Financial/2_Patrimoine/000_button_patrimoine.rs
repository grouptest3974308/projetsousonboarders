<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>000_button_patrimoine</name>
   <tag></tag>
   <elementGuidId>55287855-29eb-4deb-8a43-1cdc090d6bc1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-testid = 'assetValueRange#ToggleDropdownButton']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>assetValueRange#ToggleDropdownButton</value>
   </webElementProperties>
</WebElementEntity>
