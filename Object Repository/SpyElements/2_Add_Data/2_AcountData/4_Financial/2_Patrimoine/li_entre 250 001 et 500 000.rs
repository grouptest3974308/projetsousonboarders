<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_entre 250 001 et 500 000</name>
   <tag></tag>
   <elementGuidId>32d1ee8e-86fc-4384-9219-0fe71acb515a</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'entre 250 001€ et 500 000€' or . = 'entre 250 001€ et 500 000€')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>entre 250 001€ et 500 000€</value>
   </webElementProperties>
</WebElementEntity>
