<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_onfido_Prenez une photo</name>
   <tag></tag>
   <elementGuidId>80e8e619-012d-40a8-9717-1c12a1227639</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//button[@aria-label = 'Prendre une photo']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>653477d6-6d2d-4e85-9d1c-d46bcdba4399</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Prendre une photo</value>
      <webElementGuid>236fc43f-99a7-4cea-bb66-3943b52d77a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>onfido-sdk-ui-Camera-btn</value>
      <webElementGuid>f4d1b5dd-0b98-480f-89db-58c6956d699a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
