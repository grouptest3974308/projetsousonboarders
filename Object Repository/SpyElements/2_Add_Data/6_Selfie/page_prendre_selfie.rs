<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_prendre_selfie</name>
   <tag></tag>
   <elementGuidId>c0a1e595-3479-44ae-ba00-71254f94bccb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Prendre un selfie' or . = 'Prendre un selfie')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.onfido-sdk-ui-PageTitle-titleSpan</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='onfido-mount']/div/div/div[2]/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Prendre un selfie</value>
      <webElementGuid>2e80c8ff-17dd-4e51-913c-1392e8b92fe3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='onfido-mount']/div/div/div[2]/div/div/div/span</value>
      <webElementGuid>ca08be7d-fd25-4282-8dd5-36a3e29f0216</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Vérification d', &quot;'&quot;, 'identité')])[1]/following::span[3]</value>
      <webElementGuid>b8aef672-3448-41be-be59-ce8462554d6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quitter'])[1]/following::span[3]</value>
      <webElementGuid>58f7a7a7-6221-45fe-9c82-5d436f1aa04e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nous allons le comparer à votre pièce'])[1]/preceding::span[1]</value>
      <webElementGuid>aa371564-350c-49fb-bdf2-6a23c2dbdf0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mettez vous de face et assurez vous que vos yeux soient clairement visibles'])[1]/preceding::span[2]</value>
      <webElementGuid>52e290d9-3199-42c0-b2cb-5e63740b7b4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Prenez un selfie']/parent::*</value>
      <webElementGuid>0195fcf8-69da-4a72-82ac-f3f07f315476</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/span</value>
      <webElementGuid>cc1827b7-e9aa-4172-8f3d-ed46e9601916</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
