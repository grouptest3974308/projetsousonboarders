<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Assurez vous que lon voit bien votre tte sur votre selfie</name>
   <tag></tag>
   <elementGuidId>ea95267d-1f11-4b56-be6d-281816f1b3c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = concat('Assurez vous que l' , &quot;'&quot; , 'on voit bien votre tête sur votre selfie') or . = concat('Assurez vous que l' , &quot;'&quot; , 'on voit bien votre tête sur votre selfie'))]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.onfido-sdk-ui-Confirm-message</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Assurez vous que l'on voit bien votre tête sur votre selfie</value>
   </webElementProperties>
</WebElementEntity>
