<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_verif_selfie</name>
   <tag></tag>
   <elementGuidId>7c0c46ab-c1f0-4607-8a81-07ce7d08beb7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.onfido-sdk-ui-PageTitle-titleSpan</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Vérifier votre selfie' or . = 'Vérifier votre selfie')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Vérifier votre selfie</value>
      <webElementGuid>4a35188a-1bb6-4245-a872-964ab9c8a4c7</webElementGuid>
   </webElementProperties>
</WebElementEntity>
