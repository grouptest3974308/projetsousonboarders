<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Prenez une photo</name>
   <tag></tag>
   <elementGuidId>b82d7059-4815-4239-9606-c2939968ac28</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>IMAGE</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @aria-label = 'Prendre une photo']</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>06d8bea8-cec7-4b87-a721-6f8434d793df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Prendre une photo</value>
      <webElementGuid>c5a22243-c7a6-4a61-a31a-563c31ceff7f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
