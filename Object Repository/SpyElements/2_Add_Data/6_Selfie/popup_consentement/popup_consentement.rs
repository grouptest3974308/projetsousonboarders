<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>popup_consentement</name>
   <tag></tag>
   <elementGuidId>e16915df-3a5c-4468-9775-79a1c4fa22b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Consentement lié à la comparaison entre votre selfie et votre pièce d’identité' or . = 'Consentement lié à la comparaison entre votre selfie et votre pièce d’identité')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Consentement lié à la comparaison entre votre selfie et votre pièce d’identité</value>
   </webElementProperties>
</WebElementEntity>
