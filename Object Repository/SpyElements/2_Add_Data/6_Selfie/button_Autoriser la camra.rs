<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Autoriser la camra</name>
   <tag></tag>
   <elementGuidId>50da2e22-0138-4ca4-b9f5-307971aeb6e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and (text() = 'Autoriser la caméra' or . = 'Autoriser la caméra')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.onfido-sdk-ui-Button-button.onfido-sdk-ui-Button-button-centered.onfido-sdk-ui-Button-button-primary.onfido-sdk-ui-Button-hoverDesktop</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>onfido-sdk-ui-Button-button onfido-sdk-ui-Button-button-centered onfido-sdk-ui-Button-button-primary onfido-sdk-ui-Button-hoverDesktop</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Autoriser la caméra</value>
   </webElementProperties>
</WebElementEntity>
