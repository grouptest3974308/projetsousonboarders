<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>popup_presenceTuteur</name>
   <tag></tag>
   <elementGuidId>e33bd9bc-0c92-42d7-8279-836d3b01482d</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Présence du parent ou tuteur obligatoire' or . = 'Présence du parent ou tuteur obligatoire')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Présence du parent ou tuteur obligatoire</value>
   </webElementProperties>
</WebElementEntity>
