<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>popup_emailRecommendation</name>
   <tag></tag>
   <elementGuidId>cc15d8b5-a3ee-433c-9dc2-a199d133a304</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Profitez de l’ensemble de nos services en renseignant votre email' or . = 'Profitez de l’ensemble de nos services en renseignant votre email')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Profitez de l’ensemble de nos services en renseignant votre email</value>
   </webElementProperties>
</WebElementEntity>
