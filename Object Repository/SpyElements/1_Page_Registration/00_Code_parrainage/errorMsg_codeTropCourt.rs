<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMsg_codeTropCourt</name>
   <tag></tag>
   <elementGuidId>b6707417-2a0f-4acc-a9ea-18d289118175</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Ce code est trop court. Un code de parrainage compte 11 caractères.' or . = 'Ce code est trop court. Un code de parrainage compte 11 caractères.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ce code est trop court. Un code de parrainage compte 11 caractères.</value>
   </webElementProperties>
</WebElementEntity>
