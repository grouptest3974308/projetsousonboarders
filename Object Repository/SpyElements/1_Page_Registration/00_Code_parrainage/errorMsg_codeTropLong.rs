<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMsg_codeTropLong</name>
   <tag></tag>
   <elementGuidId>edcf7f5a-428b-48e8-a2f2-40af626a62ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Ce code est trop long. Un code de parrainage compte 11 caractères.' or . = 'Ce code est trop long. Un code de parrainage compte 11 caractères.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ce code est trop long. Un code de parrainage compte 11 caractères.</value>
   </webElementProperties>
</WebElementEntity>
