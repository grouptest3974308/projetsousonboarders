<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMsg_codeIncorrect</name>
   <tag></tag>
   <elementGuidId>56371c96-e9d0-4e86-b74a-b66ff08227cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Ce code semble incorrect. Un code de parrainage contient uniquement des chiffres et des lettres.' or . = 'Ce code semble incorrect. Un code de parrainage contient uniquement des chiffres et des lettres.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ce code semble incorrect. Un code de parrainage contient uniquement des chiffres et des lettres.</value>
   </webElementProperties>
</WebElementEntity>
