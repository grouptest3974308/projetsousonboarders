<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>validMsg_codeParrainage</name>
   <tag></tag>
   <elementGuidId>9da401a9-b3a4-45dd-9778-cdaa19a8ae86</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Votre code de parrainage a été pris en compte.' or . = 'Votre code de parrainage a été pris en compte.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Votre code de parrainage a été pris en compte.</value>
   </webElementProperties>
</WebElementEntity>
