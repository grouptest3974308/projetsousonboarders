<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>disp_invalidMail</name>
   <tag></tag>
   <elementGuidId>3509b79f-9875-4b57-84c9-5c468b938a88</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = concat('L' , &quot;'&quot; , 'adresse mail est invalide.') or . = concat('L' , &quot;'&quot; , 'adresse mail est invalide.'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>L'adresse mail est invalide.</value>
   </webElementProperties>
</WebElementEntity>
