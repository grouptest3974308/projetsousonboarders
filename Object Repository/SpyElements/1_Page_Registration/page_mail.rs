<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_mail</name>
   <tag></tag>
   <elementGuidId>1bc39bf5-8d0c-4f5f-9fd3-59370e9161d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Entrez votre adresse email :' or . = 'Entrez votre adresse email :')]</value>
      </entry>
      <entry>
         <key>IMAGE</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Entrez votre adresse email :</value>
      <webElementGuid>dddbca1d-1ff8-45c6-bb1f-4725287be128</webElementGuid>
   </webElementProperties>
</WebElementEntity>
