<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_guardian</name>
   <tag></tag>
   <elementGuidId>d2b4b04e-33f1-4be3-8c6e-920cc75f8b62</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'Êtes-vous le parent ou tuteur légal de l’enfant') or contains(., 'Êtes-vous le parent ou tuteur légal de l’enfant'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Êtes-vous le parent ou tuteur légal de l’enfant</value>
   </webElementProperties>
</WebElementEntity>
