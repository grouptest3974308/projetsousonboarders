<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_reopening-choice</name>
   <tag></tag>
   <elementGuidId>cb891ebd-2677-4fb2-9ed8-17f85eef5df9</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Avez-vous déjà eu un compte Nickel ?' or . = 'Avez-vous déjà eu un compte Nickel ?')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Avez-vous déjà eu un compte Nickel ?</value>
   </webElementProperties>
</WebElementEntity>
