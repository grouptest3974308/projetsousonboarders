<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_signing_sms-challenge</name>
   <tag></tag>
   <elementGuidId>791b6304-b949-46d2-9740-a2bc3ea3764d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Tapez le code reçu par SMS au numéro de mobile ' or . = 'Tapez le code reçu par SMS au numéro de mobile ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tapez le code reçu par SMS au numéro de mobile </value>
   </webElementProperties>
</WebElementEntity>
