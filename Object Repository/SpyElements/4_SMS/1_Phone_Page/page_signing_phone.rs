<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page_signing_phone</name>
   <tag></tag>
   <elementGuidId>09b19b88-b5bb-4944-a443-74ac50668bc8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Tapez votre numéro de mobile' or . = 'Tapez votre numéro de mobile')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tapez votre numéro de mobile</value>
   </webElementProperties>
</WebElementEntity>
