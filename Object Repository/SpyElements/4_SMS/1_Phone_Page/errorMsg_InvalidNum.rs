<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMsg_InvalidNum</name>
   <tag></tag>
   <elementGuidId>61261a4e-91d1-4b24-af45-819fc7fda55b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Numéro de téléphone invalide' or . = 'Numéro de téléphone invalide')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Numéro de téléphone invalide</value>
   </webElementProperties>
</WebElementEntity>
