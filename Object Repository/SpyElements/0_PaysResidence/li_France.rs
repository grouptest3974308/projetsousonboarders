<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_France</name>
   <tag></tag>
   <elementGuidId>917739c8-dc94-474f-83df-3b02531525d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//li[@data-testid = 'country#Option#FR']</value>
      </entry>
      <entry>
         <key>IMAGE</key>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>country#Option#FR</value>
      <webElementGuid>36b3e4a5-d710-4b29-88f9-bb8df8e7a5e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>03caace2-7e08-4fd8-aea4-91a0cca8efe3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
