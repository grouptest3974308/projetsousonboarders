import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 16/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths

//Added 18/11/2021
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions


class SOUS_02_SDK_onfido {
	@And('''l'utilisateur clique sur "Non"''')
	def clickNon() {
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/button_Non'))
		Thread.sleep(2000)
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/page_liveness_identité'), 4)
	}

	@And('''l'utilisateur clique sur "Sélectionner un document"''')
	def clickOnSelectDoc() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_selectionner_doc'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_identity_liveness'), 4)
	}

	//@validID
	//Scenario: SDK Onfido - Upload d'un passeport conforme web - P (SOUS-2550 - SOUS-2625 - SOUS-2625)
	@And('''l'utilisateur sélectionne sa pièce d'identité''')
	def selectDocID() {
		if (GlobalVariable.typeOfID == "Passeport") {
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_add_Passeport'));
			WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Passeport'), 4)
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Passeport'), 4)
		} else if (GlobalVariable.typeOfID == "Carte d'identité"){
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_add_ID_Card'))
			WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_ID_Card_recto'), 4)
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_ID_Card_recto'),4)
		} else if(GlobalVariable.typeOfID == "Carte de séjour"){
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_add_Resident_Card'))
			WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Resident_Card_recto'), 4)
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Resident_Card_recto'),4)
		}
	}

	@And('''l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce''')// #Passeport Conforme
	def clickOnSendAndUploadPhoto() {
		switch (GlobalVariable.typeOfID) {
			case "Passeport":
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Passeport'), 4)
				Path absolutePath0 = Paths.get(RunConfiguration.getProjectDir(), "Data Files/ID", "PASSEPORT_FT.jpeg");
				String path0 = absolutePath0.toString()
				println(" ******************** Type Of ID: "+ GlobalVariable.typeOfID)
				println(" ******************** Used ID: "+ path0)

				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_envoyez_photo'))

			//La page Envoyez la page du passeport contenant votre photo s'affiche
				WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/input_upload'), path0)
				Thread.sleep(2000)
			//Vérfication effectué sur le bouton et non pas sur le titre de la page => non accepté sur Xray
			//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
			//WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
				Thread.sleep(3000)
				break

			case "Carte d'identité":
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_ID_Card_recto'),4)
			//Update jpg file name if needed !!!!!!!!!!!!!!!!!!!!++++++++++++++++++++++++++++++++++++++++++
				Path absolutePath1 = Paths.get(RunConfiguration.getProjectDir(), "Data Files/ID", "ID_recto.jpg")
				Path absolutePath2 = Paths.get(RunConfiguration.getProjectDir(), "Data Files/ID", "ID_verso.jpg")
				String path1 = absolutePath1.toString()
				String path2 = absolutePath2.toString()
				println(" ******************** Type Of ID: "+ GlobalVariable.typeOfID)
				println(" ******************** Used ID 1 : "+ path1)
				println(" ******************** Used ID 2 : "+ path2)
				WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/input_upload'), path1)
				Thread.sleep(2000)

			//Vérfication effectué sur le bouton et non pas sur le titre de la page => non accepté sur Xray
			//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
			//WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
				WebUI.verifyElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
				Thread.sleep(3000)
			//Verso
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_ID_Card_verso'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_ID_Card_verso'),4)
				WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/input_upload'), path2)
				Thread.sleep(2000)
			//Vérification effectuée sur le bouton et non pas sur le titre de la page => non accepté sur Xray
			//				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
			//				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
				WebUI.verifyElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
				Thread.sleep(3000)
				break

			case "Carte de séjour":
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Resident_Card_recto'),4)
			//Update jpg file name if needed !!!!!!!!!!!!!!!!!!!!++++++++++++++++++++++++++++++++++++++++++
				Path absolutePath3 = Paths.get(RunConfiguration.getProjectDir(), "Data Files/ID", "ID_recto.jpg")
				Path absolutePath4 = Paths.get(RunConfiguration.getProjectDir(), "Data Files/ID", "ID_verso.jpg")
				String path1 = absolutePath3.toString()
				String path2 = absolutePath4.toString()
				println(" ******************** Type Of ID: "+ GlobalVariable.typeOfID)
				println(" ******************** Used ID 1 : "+ path1)
				println(" ******************** Used ID 2 : "+ path2)
				WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/input_upload'), path1)
				Thread.sleep(2000)
			//Vérification effectuée sur le bouton et non pas sur le titre de la page => non accepté sur Xray
			//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
			//WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)

				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
				Thread.sleep(3000)
			//Verso
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Resident_Card_verso'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Resident_Card_verso'),4)
				WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/input_upload'), path2)
				Thread.sleep(2000)
			//Vérification effectuée sur le bouton et non pas sur le titre de la page => non accepté sur Xray
			//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
			//WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)

				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
				Thread.sleep(3000)
		}
	}

	//@CP_ID_Forçage
	//Scenario: SDK Onfido - Forçage Upload Passeport - P (SOUS-2601)
	@And('''l'utilisateur clique sur "Recommencer"''')
	def clickOnrecommencer() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_recommencer'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
		//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)

	}
	@And('''l'utilisateur clique sur "Envoyer quand même"''')
	def clickOnEnvoieQuandMeme() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_envoyer_quand_meme'))

	}

	//********************************************************************************************
	//***********************************    @CNP_ID_tronqué   ***********************************
	//********* Scenario: SDK Onfido - Upload d'un passeport tronqué web - NP (SOUS-2552) ********
	//********************************************************************************************

	@And('''l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce tronquée''') // #Passeport tronqué
	def uploadIDTronque() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Passeport'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Passeport'), 4)
		//Update jpg file name if needed !!!!!!!!!!!!!!!!!!!!++++++++++++++++++++++++++++++++++++++++++
		Path absolutePath0 = Paths.get(RunConfiguration.getProjectDir(), "Data Files/ID", "Passeport_tronqué.jpg");
		String path0 = absolutePath0.toString()
		println(" ******************** Type Of ID: "+ GlobalVariable.typeOfID)
		println(" ******************** Used ID: "+ path0)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_envoyez_photo'))
		//La page Envoyez la page du passeport contenant votre photo s'affiche
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/input_upload'), path0)
		Thread.sleep(2000)
		//Vérification effectuée sur le bouton et non pas sur le titre de la page => non accepté sur Xray
		//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
		//WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)

		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)

		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
		Thread.sleep(3000)
	}

	//**************************************************************************************************
	//***********************************    @CNP_ID_flou   ********************************************
	//********* Scenario: SDK Onfido - Upload d'une carte d'identité Floue web - NP (SOUS-2627) ********
	//**************************************************************************************************

	@And('''l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce floue''') //#Passeport floue
	def uploadIDflou() {
		//		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_ID_Card_recto'), 4)
		//		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_ID_Card_recto'),4)
		//Update jpg file name if needed !!!!!!!!!!!!!!!!!!!!++++++++++++++++++++++++++++++++++++++++++
		Path absolutePath1 = Paths.get(RunConfiguration.getProjectDir(), "Data Files/ID", "ID_recto_floue.jpg")
		String path1 = absolutePath1.toString()
		//WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_envoyez_photo'))

		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/input_upload'), path1)
		Thread.sleep(2000)
		//Vérification effectuée sur le bouton et non pas sur le titre de la page => non accepté sur Xray
		//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
		//WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)

		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
		Thread.sleep(3000)
	}


	//***************************************************************************************************
	//***********************************    @CNP_ID_reflet   *******************************************
	//***** Scenario: SDK Onfido - Upload d'un titre de séjour recto reflet web - NP (SOUS-2556) ********
	//***************************************************************************************************

	@And('''l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce avec reflet''') // #Passeport reflet
	def uploadIDReflet() {
		//		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_add_Resident_Card_recto'),4)
		//Update jpg file name if needed !!!!!!!!!!!!!!!!!!!!++++++++++++++++++++++++++++++++++++++++++
		Path absolutePath3 = Paths.get(RunConfiguration.getProjectDir(), "Data Files/ID", "Titre_sejour_recto_reflet.PNG")
		String path1 = absolutePath3.toString()
		//WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_envoyez_photo'))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/input_upload'), path1)
		Thread.sleep(2000)
		//Vérification effectuée sur le bouton et non pas sur le titre de la page => non accepté sur Xray
		//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)
		//WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/page_verifiez_votre_image'), 4)

		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'), 4)

		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_onfido_envoyez_img'))
		Thread.sleep(3000)
	}

	@Then('''un message d'erreur "Image tronquée" apparait''')
	def errorMsg1() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/error_msg_tronquée'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/error_msg_tronquée'),4)
	}

	@Then('''un message d'erreur "Image floue" apparait''')
	def errorMsg2() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/error_msg_floue'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/error_msg_floue'),4)
	}

	@Then('''un message d'erreur "Image avec reflet" apparait''')
	def errorMsg3() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/error_msg_reflet'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/error_msg_reflet'),4)
	}

	@And('''le bouton "Recommencer" est présent''')
	def buttonRestartDisp() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/5_Upload/button_recommencer'),4)
	}
}
