import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added on 22/10 for performance test
import java.util.concurrent.TimeUnit
import org.apache.commons.lang3.time.StopWatch


class SOUS_01_AjoutMail {
	// Background
	@Given('''l'utilisateur sélectionne le pays de souscription et clique sur Suivant''')
	def navigateToLoginPage() {
		WebUI.openBrowser("https://welcome.staging.lfpe.fr/")
		boolean status =  WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_Tout accepter'),4, FailureHandling.OPTIONAL)
		println(status)

		if ( status == false) {
			println("Bouton Tout accepter ne s'affiche pas")
		} else {
			println("Bouton Tout accepter s'affiche")
			WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_Tout accepter'))
		}

		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/page_registration-fr'), 4)
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_dansQuelPays'), 4)

		switch (GlobalVariable.souscriptionCountry) {
			case "France":
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_Suivant'))
				break
			case "Portugal":
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_dansQuelPays'))
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_Portugal'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_Portugal'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_Portugal'))
				Thread.sleep(2000)
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_Suivant'))
			case "Belgique":
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_dansQuelPays'))
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_Belgique'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_Belgique'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_Belgique'))
				Thread.sleep(2000)
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_language'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_language'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_language'))
				switch (GlobalVariable.souscriptionLanguage) {
					case "Français":
						WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_lang_Franais'), 4)
						WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_lang_Franais'))
						break
					case "Nederlands":
						WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_lang_Nederlands'), 4)
						WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_lang_Nederlands'))
						break
					case "English":
						WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_lang_English'), 4)
						WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_lang_English'))
						break
				}
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_Suivant'))
				break
			case "Espagne":
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_dansQuelPays'))
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_Espagne'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_Espagne'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/li_Espagne'))
				Thread.sleep(2000)
				WebUI.click(findTestObject('Object Repository/SpyElements/0_PaysResidence/button_Suivant'))
				break
		}

		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_AllElement'), 4)
	}


	@When('''l'utilisateur clique sur "Ouvrir un compe en 5 minutes"''')
	def ouvrirCmp5mn() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_AllElement'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/button_Ouvrir un compte en 5 minutes'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_mail'), 4)
	}


	//@withoutEmail Scenario: l'utilisateur continue sans renseigner une adresse mail
	@Then('''la popup "Recommendation mail" s'affiche''')
	def popupMailDisp() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/1_Page_Registration/popup_emailRecommendation'), 4)
	}
	@When('''l'utilisateur clique sur "Renseigner mon email"''')
	def enterEmailClick() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/popup_emailRecommendation'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/button_Renseigner mon email'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_mail'), 4)
	}
	@Then('''l'utilisateur est de nouveau sur la page "Facile en 5 minutes"''')
	def facilePage() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_mail'), 4)
	}

	@When('''l'utilisateur clique sans renseigner le mail sur "Démarrer"''')
	def clickOnDemarrer() {
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/button_Dmarrer'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/1_Page_Registration/popup_emailRecommendation'), 4)
	}

	@When('''l'utilisateur clique sur "Continuer sans renseigner"''')
	def continueWithoutEmail() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/popup_emailRecommendation'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/button_withoutEmail'))
	}

	//@validEmail Scenario Outline: l'utilisateur renseigne une adresse mail valide
	@When('''l'utilisateur renseigne une adresse "e-mail" valide''')
	def enterValidEmail() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/1_Page_Registration/input_mail'),GlobalVariable.email)
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_mail'))
	}

	@And('''l'utilisateur clique sur "Démarrer"''')
	def clickDemarrer() {
		Thread.sleep(5000)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/button_Dmarrer'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/button_Dmarrer'))
	}


	@Then('''la page "Avez-vous déjà eu un compte Nickel ?" s'affiche''')
	def verifyAvezVousComptePage() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_reopening-choice'), 4)

	}
	//@InvalidEmail_hote_inconnu
	@When('''l'utilisateur renseigne une adresse "e-mail" avec hote inconnu''')
	def enterUnknownHost() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/1_Page_Registration/input_mail'),findTestData('Invalid_personal_data').getValue(4, 1))
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_mail'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/1_Page_Registration/disp_invalidMail'), 4)
	}

	//@InvalidEmail_structure_invalide
	@When('''l'utilisateur renseigne une adresse "e-mail" avec structure invalide''')
	def enterInvalidEmailStructure() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/1_Page_Registration/input_mail'),findTestData('Invalid_personal_data').getValue(4, 2))
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_mail'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/1_Page_Registration/disp_invalidMail'), 4)
	}

	//@InvalidEmail_utilisateur_inconnu
	@When('''l'utilisateur renseigne une adresse "e-mail" avec utilisateur inconnu''')
	def enterInvalidUserName() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/1_Page_Registration/input_mail'),findTestData('Invalid_personal_data').getValue(4, 3))
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/page_mail'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/1_Page_Registration/disp_invalidMail'), 4)


		//create a started stopwatch for performance test
		//StopWatch stopWatch = StopWatch.createStarted()
		//Element to check
		//WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/disp_invalidMail'), 4)
		//Stop the stopwatch
		//stopWatch.stop()
		//println("${stopWatch.getTime(TimeUnit.MILLISECONDS) / 1000} seconds")
		//println('''"Adresse mail invalide" s'affiche après '''+"${stopWatch.getTime(TimeUnit.MILLISECONDS) / 1000} seconds")
		//if ("${stopWatch.getTime(TimeUnit.MILLISECONDS) / 1000}" <= 0.5) {
		//	println("**************** Le temps d'affichage du message d'alerte est inférieur à 0.5s ****************")
		//} else {
		//	println("**************** Le temps d'affichage du message d'alerte dépasse 0.5s ****************")
		//}

		//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/1_Page_Registration/disp_invalidMail'), 4)
	}

	@Then('''Le message d'erreur "Adresse mail invalide" s'affiche''')
	def verifyPage() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/disp_invalidMail'), 4)
	}


	@Then('''Le message d'erreur "Adresse mail invalide" s'affiche et l'évaluation du temps d'affichage est établie''')
	def verifyPageAndPerformance() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/disp_invalidMail'), 4)


		//create a started stopwatch for performance test
		StopWatch stopWatch = StopWatch.createStarted()
		//Element to check
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/disp_invalidMail'), 4)
		//Stop the stopwatch
		stopWatch.stop()
		println("${stopWatch.getTime(TimeUnit.MILLISECONDS) / 1000} seconds")
		println('''"Adresse mail invalide" s'affiche après '''+"${stopWatch.getTime(TimeUnit.MILLISECONDS) / 1000} seconds")
		if ("${stopWatch.getTime(TimeUnit.MILLISECONDS) / 1000}" <= 0.5) {
			println("**************** Le temps d'affichage du message d'alerte est inférieur à 0.5s ****************")
		} else {
			println("**************** Le temps d'affichage du message d'alerte dépasse 0.5s ****************")
		}


	}
}