import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys



class SOUS_09_CDG_Signature {
	//  Background: Siganture des conditions générales et tarifaires
	@Given('''la page des conditions générales "CDG" s'affiche''')
	def pageCDG_Disp() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/3_Page_CGD/page_signing_terms-and-conditions'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/3_Page_CGD/page_signing_terms-and-conditions'), 4)
	}
	@And('''la vérification des contrats est OK''')
	def verifDocCDG() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/3_Page_CGD/CDG_Doc1'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/3_Page_CGD/CDG_Doc2'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/3_Page_CGD/CDG_Doc3'), 4)
		String Text_URL1= ""
		String Text_URL2= ""
		String Text_URL3= ""

		switch (GlobalVariable.souscriptionCountry) {
			case "France":
				Text_URL1= "https://docs.google.com/gview?rm=minimal&widget=false&ui=2&embedded=true&url=https://nickel.eu/sites/default/files/General-Terms-and-Conditions_FR_fr.pdf?locale=fr-FR#toolbar=0"
				Text_URL2= "https://docs.google.com/gview?rm=minimal&widget=false&ui=2&embedded=true&url=https://nickel.eu/sites/default/files/Insurance-Policy-Nickel_FR_fr.pdf?locale=fr-FR#toolbar=0"
				Text_URL3= "https://docs.google.com/gview?rm=minimal&widget=false&ui=2&embedded=true&url=https://nickel.eu/sites/default/files/Insurance-Policy-Nickel-Chrome-Metal_FR_fr.pdf?locale=fr-FR#toolbar=0"
				break
			case "Portugal":
				Text_URL1= "https://nickel.eu/sites/default/files/General-Terms-and-Conditions_PT_pt.pdf"
				Text_URL2= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel_PT_pt.pdf"
				Text_URL3= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel-Chrome-Metal_PT_pt.pdf"
				break
			case "Belgique":
				if(GlobalVariable.souscriptionLanguage == "Français") {
					Text_URL1= "https://nickel.eu/sites/default/files/General-Terms-and-Conditions_BE_fr.pdf"
					Text_URL2= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel_BE_fr.pdf"
					Text_URL3= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel-Chrome-Metal_BE_fr.pdf"
				} else if(GlobalVariable.souscriptionLanguage == "Nederlands"){
					Text_URL1= "https://nickel.eu/sites/default/files/General-Terms-and-Conditions_BE_nl.pdf"
					Text_URL2= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel_BE_nl.pdf"
					Text_URL3= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel-Chrome-Metal_BE_nl.pdf"
				}else if(GlobalVariable.souscriptionLanguage == "English"){
					Text_URL1= "https://nickel.eu/sites/default/files/General-Terms-and-Conditions_BE_en.pdf"
					Text_URL2= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel_BE_en.pdf"
					Text_URL3= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel-Chrome-Metal_BE_en.pdf"
				}
				break
			case "Espagne":
				Text_URL1= "https://nickel.eu/sites/default/files/General-Terms-and-Conditions_ES_es.pdf"
				Text_URL2= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel_ES_es.pdf"
				Text_URL3= "https://nickel.eu/sites/default/files/Insurance-Policy-Nickel-Chrome-Metal_ES_es.pdf"
				break
		}


		//Vérification du Doc2
		WebUI.click(findTestObject('Object Repository/SpyElements/3_Page_CGD/CDG_Doc2'))
		String URL2= WebUI.getAttribute(findTestObject('Object Repository/SpyElements/3_Page_CGD/URL_Doc2'), 'src')
		WebUI.verifyEqual(URL2, Text_URL2, FailureHandling.STOP_ON_FAILURE)
		Thread.sleep(3000)


		//Vérification du Doc3
		WebUI.click(findTestObject('Object Repository/SpyElements/3_Page_CGD/CDG_Doc3'))
		String URL3= WebUI.getAttribute(findTestObject('Object Repository/SpyElements/3_Page_CGD/URL_Doc3'), 'src')
		WebUI.verifyEqual(URL3, Text_URL3,  FailureHandling.STOP_ON_FAILURE)
		Thread.sleep(2000)

		//Vérification du Doc1
		WebUI.click(findTestObject('Object Repository/SpyElements/3_Page_CGD/CDG_Doc1'))
		String URL1= WebUI.getAttribute(findTestObject('Object Repository/SpyElements/3_Page_CGD/URL_Doc1'), 'src')
		WebUI.verifyEqual(URL1, Text_URL1,  FailureHandling.STOP_ON_FAILURE)
		Thread.sleep(2000)
	}
	//@VerifCDG_NePasAccepter	Scenario: #1
	@When('''l'utilisateur signe le contrat''')
	def signContract() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/3_Page_CGD/button_Signer'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/3_Page_CGD/button_Signer'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/3_Page_CGD/button_Signer'));
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/page_signing_phone'), 4)
	}

	@Then('''la page mobile "Tapez votre numéro de mobile" s'affiche''')
	def mobilePageDisp() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/page_signing_phone'),4)
	}

	//@VerifCDG_Nickel	Scenario: #2
	@When('''l'utilisateur clique sur "Accepter que les données soient utilisées par Nickel"''')
	def acceptDataNickel() {
		WebUI.click(findTestObject('Object Repository/SpyElements/3_Page_CGD/01_Agree_data_Nickel'));
		Thread.sleep(2000)
	}
	//@VerifCDG_Partners	Scenario: #3
	@When('''l'utilisateur clique sur "Accepter que les données soient utilisées par les partenaires"''')
	def acceptDataPartners() {
		WebUI.click(findTestObject('Object Repository/SpyElements/3_Page_CGD/02_Agree_data_partenaires'));
		Thread.sleep(2000)
	}
	//@VerifCDG_Nickel&Partners 	Scenario: #4
}