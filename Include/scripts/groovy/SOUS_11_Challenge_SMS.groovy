import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys

//Added 17/09/2021
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import groovy.json.JsonSlurper as JsonSlurper


class SOUS_11_Challenge_SMS {

	@When('''l'utilisateur renseigne le challenge SMS''')
	def addSMSChallenge() {
		println ("+++++++++++++++++++++ First name (for Challenge SMS):"+ GlobalVariable.firstName.toUpperCase() + "++++++++++++++++++++++++++++")
		println ("+++++++++++++++++++++ Last name  (for Challenge SMS) :"+ GlobalVariable.lastName.toUpperCase()+ "++++++++++++++++++++++++++++")

		def responseAPI = WS.sendRequest(findTestObject('Object Repository/Challenge_SMS'))
		WS.verifyResponseStatusCode(responseAPI, 200)
		def slurper = new JsonSlurper()
		def resultAPI = slurper.parseText(responseAPI.getResponseBodyContent())
		def String challengeSMS
		int index = 0
		int nbFound = 0

		while (index < resultAPI.size && resultAPI[index].lastName != null && resultAPI[index].firstName != null) {
			println("++++++++++++++++++  index  = "+index)
			String lastNameAPIstr  = resultAPI[index].lastName.toUpperCase()
			String firstNameAPIstr = resultAPI[index].firstName.toUpperCase()
			lastNameAPIstr= lastNameAPIstr.replace("_"," ")
			firstNameAPIstr= firstNameAPIstr.replace("_"," ")
			println("resultat 1 ="+ lastNameAPIstr.trim())
			println("resultat 2 ="+ GlobalVariable.lastName.toUpperCase())
			println("resultat 3 ="+ firstNameAPIstr.trim()  )
			println("resultat 4 ="+ GlobalVariable.firstName.toUpperCase())


			if(lastNameAPIstr.trim() == GlobalVariable.lastName.toUpperCase()) {
				if(firstNameAPIstr.trim() == GlobalVariable.firstName.toUpperCase()){
					println("Index *********************"+ index  )
					println("SMS *********************"+  resultAPI[index].sms3  )
					challengeSMS = resultAPI[index].sms3
					println(" ********************* ChallengeSMS: "+  challengeSMS )
					index = resultAPI.size
					nbFound += 1
				}}
			index+=1
		}

		if(nbFound == 0){
			KeywordUtil.markError('''************************* SMS Challenge non retrouvé *************************''')
		}
		//println("challengeSMS 2 *********************" +  challengeSMS  )
		//		Thread.sleep(4000)
		//Added on 21/10/21 Ridhoini & Charlie => setText doesn't work
		WebUI.executeJavaScript("document.getElementById('challenge').value = '"+challengeSMS+"' ;", null)
		WebUI.executeJavaScript("document.getElementById('challenge').dispatchEvent(new Event('input'));", null)

		//	Thread.sleep(4000)
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/button_Suivant'),4)
	}
	@And('''l'utilisateur valide le challenge SMS en cliquant sur "Suivant"''')
	def SMSvalidation() {
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/4_SMS/3_Bravo/page_final'), 4)
	}
	@Then('''la réouverture de compte a été prise en compte''')
	def reopenCmp() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/4_SMS/3_Bravo/page_final'), 4)
	}

	@Then('''le code à 5 chiffres affiché correspond au code d'activation reçu par SMS''')
	def checkCodeActiv() {
		def responseAPI = WS.sendRequest(findTestObject('Object Repository/Challenge_SMS'))
		WS.verifyResponseStatusCode(responseAPI, 200)
		def slurper = new JsonSlurper()
		def resultAPI = slurper.parseText(responseAPI.getResponseBodyContent())
		def String activationCodeAPI
		String activationCodeDisplayed = WebUI.getText(findTestObject('Object Repository/SpyElements/4_SMS/3_Bravo/activationCode'))
		def String challengeSMS
		int index = 0
		int nbFound = 0




		while (index < resultAPI.size && resultAPI[index].lastName != null && resultAPI[index].firstName != null) {
			println("++++++++++++++++++  index  = "+index)
			String lastNameAPIstr  = resultAPI[index].lastName.toUpperCase()
			String firstNameAPIstr = resultAPI[index].firstName.toUpperCase()
			lastNameAPIstr= lastNameAPIstr.replace("_"," ")
			firstNameAPIstr= firstNameAPIstr.replace("_"," ")
			println("resultat 1 ="+ lastNameAPIstr.trim())
			println("resultat 2 ="+ GlobalVariable.lastName.toUpperCase())
			println("resultat 3 ="+ firstNameAPIstr.trim()  )
			println("resultat 4 ="+ GlobalVariable.firstName.toUpperCase())

			if(lastNameAPIstr.trim() == GlobalVariable.lastName.toUpperCase()) {
				if(firstNameAPIstr.trim() == GlobalVariable.firstName.toUpperCase()){
					println("Index *********************"+ index  )
					println("SMS *********************"+  resultAPI[index].sms5  )
					activationCodeAPI = resultAPI[index].sms5
					println(" ********************* Activation code: "+  activationCodeAPI )
					index = resultAPI.size
					nbFound += 1
				}}
			index+=1
		}

		if(nbFound == 0){
			KeywordUtil.markError('''************************* Code d'activation à 5 chiffres non retrouvé *************************''')

		}

		if (activationCodeAPI == activationCodeDisplayed){
			println("********************* Le code d'activation à 5 chiffre a été vérifié avec succès *********************"  )
			println("********************* Code d'activation affiché sur l'interface : "+ activationCodeDisplayed )
			println("********************* Code d'activation généré par l'API : "+ activationCodeAPI )
		}else {
			KeywordUtil.markError('''************************* Code d'activation affiché différent du code d'activation généré par l'API *************************''')
			println("********************* Code d'activation affiché sur l'interface : "+ activationCodeDisplayed )
			println("********************* Code d'activation généré par l'API : "+ activationCodeAPI )
		}
	}
}