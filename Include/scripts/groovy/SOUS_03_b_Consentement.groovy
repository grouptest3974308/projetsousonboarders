import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 16/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths

//Added 18/11/2021
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions



class SOUS_03_b_Consentement {
	@Given('''le pop-up "Consentement lié à la comparaison entre votre selfie et votre pièce d’identité" s'affiche''')
	def popupConsentDisplay() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/popup_consentement/popup_consentement'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/popup_consentement/popup_consentement'), 4)

	}


	//@AcceptConsent
	//	Scenario: #1 Vérifier qu'il est possible de passer à l'étape suivante en acceptant le consentement biométrique
	@When('''l'utilisateur clique sur le bouton "Accepter et continuer"''')
	def acceptConsent() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/popup_consentement/button_continueAccept'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/popup_consentement/button_continueAccept'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/popup_consentement/button_continueAccept'))
	}


	//	@RefusConsent
	//	Scenario: #2 vérifier qu'il est possible de passer à l'étape suivante en refusant le consentement biométrique
	@When('''l'utilisateur clique sur le bouton "Continuer sans accepter"''')
	def rejectConsent() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/popup_consentement/button_continueWithoutAccept'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/popup_consentement/button_continueWithoutAccept'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/popup_consentement/button_continueWithoutAccept'))
	}

	//	@CloseConsent
	//	Scenario: #3 Vérifier qu'il est possible de passer à l'étape suivante en fermant le consentement biométrique
	@When('''l'utilisateur clique sur la "Croix"''')
	def closeConsent() {
		//WebUI.verifyElementPresent(findTestObject('//MissingID'), 4)
		//WebUI.click(findTestObject('//MissingID'))
	}

	@Then('''le pop-up disparait''')
	def popupConsentDisappear() {
		WebUI.verifyElementNotVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/popup_consentement/popup_consentement'), 4)
	}

}