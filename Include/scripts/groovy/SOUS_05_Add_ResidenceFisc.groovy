import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys


class SOUS_05_Add_ResidenceFisc {

	//@RésidenceFiscaleUniqueFr
	//Scenario: #1
	@When('''l'utilisateur clique sur Oui''')
	def clickYesRFfr() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/oui'))
		Thread.sleep(2000)
	}
	@And('''l'utilisateur clique sur suivant''')
	def clickNextRf() {
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/page_us-person'), 4)
	}

	@Then('''la page citoyen US s'affiche''')
	def citoyenUSpage() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/page_us-person'), 4)
	}
	//@AjoutRésidenceFiscale2
	//Scenario: #2
	@When('''l'utilisateur clique sur Non''')
	def clickNonRFfr() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/non'))
		Thread.sleep(2000)
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/popup_numIDFisc_a_fournir'), 4)
	}
	@Then('''la popup NumIdFisc s'affiche''')
	def popupDispNumFisc() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/popup_numIDFisc_a_fournir'), 4)
	}

	@When('''l'utilisateur clique sur J'ai compris''')
	def clickCompris() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/button_compris'))
	}

	@And('''l'utilisateur renseigne une résidence fiscale UE_AELE''')
	def addSecondRF() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/button_RF'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/button_RF'))
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/input_RF'), GlobalVariable.residenceFiscale2)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/input_RF'), Keys.chord(Keys.DOWN))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/input_RF'), Keys.chord(Keys.ENTER))

		//WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/page_tax-residence'))
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/button_Suivant'), 4)

		//Thread.sleep(2000)
	}

	//@SupprimerRésidenceFiscale2
	//Scenario: #3
	@When('''l'utilisateur supprime sa deuxième résidence fiscale''')
	def deleteSecondRF() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/delete_RF2'))
		Thread.sleep(2000)
	}

	@Then('''la popup Résidences fiscales s'affiche''')
	def popupRFDisp() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/popup_RF_2'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/button_RF_Only_FR'), 4)
	}
	@When('''l'utilisateur clique sur Résidence fisc uniquement en Fr''')
	def clickRFonlyFr() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/button_RF_Only_FR'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/page_us-person'), 4)

	}

	@When('''l'utilisateur clique sur Revenir au formulaire''')
	def revenirFormulaire() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/button_Revenir formulaire_citoyenETresident'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/page_tax-residence'), 4)
	}

	//@RésidenceFiscaleHorsUE
	//Scenario: #4
	@And('''l'utilisateur supprime sa première résidence fiscale''')
	def deleteFirstRF() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/delete_RF1'))
		Thread.sleep(2000)
	}
	@And('''l'utilisateur renseigne une résidence fiscale hors UE_AELE''')
	def addRFHorsUEAELE() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/button_RF'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/button_RF'))
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/input_RF'), findTestData('Invalid_personal_data').getValue(9, 10) )
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/input_RF'), Keys.chord(Keys.DOWN))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/input_RF'), Keys.chord(Keys.ENTER))
	}

	@Then('''le message "Nickel n’est pas disponible pour les clients ayant des résidences fiscales hors UE ou AELE" s'affiche''')
	def errorMsgPaysHorsUEAELE() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/errorMessage_PaysHorsUEAELE'), 4)
	}


	//@RésidenceFiscaleHors_UE_AELE_MSC
	//Scenario: #5
	@And('''l'utilisateur renseigne une résidence fiscale hors UE_AELE_MSC''')
	def addRFMSC() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/button_RF'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/button_RF'))
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/input_RF'), findTestData('Invalid_personal_data').getValue(9, 11) )
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/input_RF'), Keys.chord(Keys.DOWN))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/input_RF'), Keys.chord(Keys.ENTER))
	}

	@And('''le bouton suivant est grisé''')
	def greyButtonNext() {
		WebUI.verifyElementNotClickable(findTestObject('''Object Repository/SpyElements/button_Suivant'''))
	}


}
