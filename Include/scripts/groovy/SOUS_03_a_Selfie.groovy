import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 16/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths

//Added 18/11/2021
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions

class SOUS_03_a_Selfie {
	@Then('''la page "Prenez un selfie" s'affiche''')
	def selfiePageCheck(){
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/page_prendre_selfie'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/page_prendre_selfie'), 4)
	}
	@When('''l'utilisateur clique sur "Continuer"''')
	def clickOnContinue() {
		println("Start - Continue -")
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_Continuer'))
		Thread.sleep(2000)
		println("End - Continue -")
		//WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/page_selfie_GardezVisage'), 4)
	}

	//@UploadID_Selfie
	//Scenario: #1 Upload AND se prendre en selfie
	@When('''l'utilisateur se prend en photo et clique sur "Envoyer"''')
	def takeSelfie(){
		//WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/page_selfie_GardezVisage'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_onfido_Prenez une photo'), 4)
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_onfido_Prenez une photo'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/page_verif_selfie'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_RestartSelfie'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_Envoyer'))
	}

	@Then('''la page "Quelle est votre situation familiale ?" s'affiche''')
	def checkKYCPage(){
		WebUI.verifyElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/00_Situation_Familiale/page_family_status'))
	}

	//@UploadID_Restart_Selfie
	//Scenario: #2 Upload AND Recommencer un selfie
	@When('''l'utilisateur se prend en photo et clique sur "Recommencer"''')
	def restartSelfie(){
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_onfido_Prenez une photo'), 4)
		WebUI.verifyElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_onfido_Prenez une photo'))
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_onfido_Prenez une photo'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/page_verif_selfie'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_RestartSelfie'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/6_Selfie/button_RestartSelfie'))
	}
}
