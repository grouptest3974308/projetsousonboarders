import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys

class SOUS_06_CitoyenUS {
	//************************************************************************* Background **********************************************************************
	@Given('''l'utilisateur est sur la page Citoyen US''')
	def usageSectionDisp() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/page_us-person'), 4)
		Thread.sleep(2000)
	}

	//*************************************************** @NonCitoyenUS *****************************************************************************************
	//*************************************************** Scenario: l'utilisateur n'est pas citoyen américain ***************************************************

	@When('''l'utilisateur clique sur "Non" _US''')
	def clickNon() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/Citoyen_US_NON'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/Citoyen_US_NON'))
		Thread.sleep(2000)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/05_Confirm_Info/page_private-life-certification'), 4)
	}

	//********************************************************************** @CitoyenUSETRésident_BackToForm **********************************************************************
	//********************************************************************** Scenario: l'utilisateur est citoyen et résident américain ********************************************
	@When('''l'utilisateur clique sur "Oui" _US''')
	def clickOui() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/Citoyen_US_OUI'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/Citoyen_US_OUI'))
		Thread.sleep(2000)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_OUI'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_NON'), 4)
	}

	@And('''l'utilisateur clique sur "Citoyen ou résident"''')
	def citoyenAndResident() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_OUI'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_NON'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_OUI'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/div_popup_Dsol_citoyenETresident'), 4)
	}
	@Then('''la popup "Client non accepté" s'affiche''')
	def popupNonAccepted() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/div_popup_Dsol_citoyenETresident'), 4)
	}
	@Then('''l'utilisateur clique sur "Revenir au formulaire"''')
	def clickRevenirSurLeFormulaire() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/button_Revenir formulaire_citoyenETresident'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_OUI'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_NON'), 4)
	}
	@And('''l'utilisateur clique sur "Né et non citoyen" _US''')
	def clickNeAndNonCitoyen() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_NON'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/div_Popup_Document fournir_nonCitoyen'), 4)
	}
	@Then('''la popup de document à fournir "Document à fournir" s'affiche''')
	def popupDocAFournir() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/div_Popup_Document fournir_nonCitoyen'), 4)
	}
	@And('''l'utilisateur clique sur "J'ai compris" _MAJSituation''')
	def clickOnJaiCompris() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/button_Jai compris'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_OUI'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/fatca_NON'), 4)
	}

	//****************************************************** @CitoyenUSETRésident_Cancel *********************************************************************
	//Scenario: l'utilisateur est citoyen et résident américain => Annnulation de l'ouverture du compte ******************************************************
	@Then('''l'utilisateur annule l'ouverture de son compte en cliquant sur "Annuler l'ouverture de mon compte"''')
	def annulationOuvertureCmp() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/div_popup_Dsol_citoyenETresident'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/button_Annuler_citoyenETresident'))
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/0_PaysResidence/page_registration-fr'), 4)
	}
}