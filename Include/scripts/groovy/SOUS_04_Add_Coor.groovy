import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys

//Added 17/09/2021
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import groovy.json.JsonSlurper as JsonSlurper

class SOUS_04_Add_Coor {
	//*********************** Background:***********************
	@Given('''la page "Saisissez votre adresse postale" s'affiche''')
	def coordPageDisp() {
		println("**************** Page Adresse postale dispalayed <Début>")
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/page_adresse'), 4)
		Thread.sleep(3000)
		println("**************** Page Adresse postale dispalayed <Fin>")

	}
	// *********************** @AddGoogAdd ***********************
	// Scenario: Saisie via Google Adresse
	@When('''l'utlisateur saisi son adresse''')
	def addAdress() {
		//| 1 Rue Saint-Lazare, Paris, France |
		// 17/06/2022
		//	1 Rue Saint-Lazare, Paris, France
		String GoogAddress =WebUI.concatenate([
			GlobalVariable.NumRue ,
			" ",
			GlobalVariable.NomRue,
			", ",
			GlobalVariable.ville,
			", ",
			GlobalVariable.pays] as String[], FailureHandling.STOP_ON_FAILURE)
		println("Before click")
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'))
		println("After click")
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'),GoogAddress)
		println("Google address set")
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'))

		//	WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'), Keys.chord(Keys.DOWN))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'), Keys.chord(Keys.ENTER))

		//		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/googAddress_1stListElement'))
		println("Google address selected")
		String addedGoogAddress = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'), 'value')
		println("******************* addedGoogAddress : "+ addedGoogAddress)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/page_adresse'))
		println("*************** Google Adresse : "+ GoogAddress)
		Thread.sleep(3000)

	}
	@And('''l'utilisateur renseigne complément d'adresse''')
	def AddCompAdresse() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_additionalInfo'), 4)
		println("*************** Complément d'adresse : "+ GlobalVariable.CompAdresse)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_additionalInfo'),GlobalVariable.CompAdresse)
		Thread.sleep(2000)
	}
	@When('''l'utilisateur clique sur "Suivant"''')
	def clickNext2() {
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
	}

	@Then('''la page tax_residence s'affiche''')
	def situationSectionDisp() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/page_tax-residence'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/page_tax-residence'), 4)
		Thread.sleep(3000)
	}
	// @AddManAdd
	//Scenario: Saisie manuelle
	@When('''l'utilisateur clique sur "saisir manuellement"''')
	def addAddressMan() {

		println("**************** Saisie manuelle <Début>")
		String newVal1 = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/button_adresse manuellement'),'data-testid')
		println("++++++++++++++++++++++++ newVal :" + newVal1)
		String newVal2 = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/button_adresse manuellement'),'value')


		println("++++++++++++++++++++++++ newVal :" + newVal2)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/button_adresse manuellement'))

		Thread.sleep(1000)
		//WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/page_adresse'), 4)
		println("**************** Saisie manuelle <Fin>")
	}
	@When('''l'utilisateur renseigne code postal''')
	def addCodePostal() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_codePostal'), 4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_codePostal'), GlobalVariable.zipCode)
		println("*************** Code postal : " + GlobalVariable.zipCode)
	}
	@And('''l'utilisateur renseigne ville''')
	def addVille() {
		// Si pays = France => le champs ville est pré rempli => Renseigner la ville que si Pays != France
		if(GlobalVariable.pays != "France" ) {
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_ville'), 4)
			WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_ville'),GlobalVariable.ville)
			println("*************** Ville : "+GlobalVariable.ville)
		}
	}
	@And('''l'utilisateur renseigne numéro de rue''')
	def addNumRue() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_streetNumber'), 4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_streetNumber'),GlobalVariable.NumRue)
		println("*************** NumRue : "+GlobalVariable.NumRue)
	}
	@And('''l'utilisateur renseigne nom de rue''')
	def AddNomRue() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_streetName'), 4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_streetName'), GlobalVariable.NomRue)
		println("*************** NomRue : "+GlobalVariable.NomRue)
	}

	//  @AddWithoutNumRue_MoreThan32Character
	//Scenario: Saisie manuelle_Sans Numéro de rue_Nom de rue et complément d'adresse contennant plus de 32 caractères
	@And('''l'utilisateur renseigne nom de rue contenant plus de 32 caractères''')
	def AddNomRueMoreThan32() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_streetName'), 4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_streetName'), findTestData('Invalid_personal_data').getValue(7, 9) )
		//	Thread.sleep(4000)
	}
	@And('''l'utilisateur renseigne complément d'adresse contenant plus de 32 caractères''')
	def AddCompAdresseMoreThan32() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_additionalInfo'), 4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_additionalInfo'), findTestData('Invalid_personal_data').getValue(8, 9))
		Thread.sleep(4000)
	}

	//@FocusAddressCheck
	//Scenario: Vérifier que le focus sur l'adresse ne se perd pas si on clique ailleurs
	@When('''l'utlisateur commence à saisir son adresse et clique ailleurs''')
	def checkFocusNotLost() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'),"1")
		Thread.sleep(2000)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'), Keys.chord(Keys.DOWN))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'), Keys.chord(Keys.ENTER))
		GlobalVariable.GoogAddress = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'), 'value')
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/page_adresse'))
		println("+++++++++++++++++ GoogAddress GlobalVar = " + GlobalVariable.GoogAddress)

	}
	@Then('''le champ adresse ne se vide pas''')
	def checkAddressFieldIsNotEmpty() {
		Thread.sleep(2000)
		String GoogAddressAfterCLick= WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_googAddress'), 'value')
		println("+++++++++++++++++ GoogAddressAfterCLick = "+ GoogAddressAfterCLick)
		Thread.sleep(2000)


		String NewGoogAddress =WebUI.concatenate([
			GlobalVariable.NumRue ,
			" ",
			GlobalVariable.NomRue,
			", ",
			GlobalVariable.zipCode,
			" ",
			GlobalVariable.ville,
			", ",
			GlobalVariable.pays] as String[], FailureHandling.STOP_ON_FAILURE)

		println("Le nouveau format de l'adresse : " + NewGoogAddress)
		WebUI.verifyMatch(NewGoogAddress, GoogAddressAfterCLick, false, FailureHandling.STOP_ON_FAILURE)
	}


	//@ValidationAddressAvecRetour
	@And('''l'utilisateur clique sur "Retour"''')
	def clickOnBack() {
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Retour'))
	}

	@And('''le champ Complément d'adresse ne se vide pas''')
	def checkComplementAddressFieldIsNotEmpty() {
		Thread.sleep(2000)
		String ComplementAddress= WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/input_additionalInfo'), 'value')
		println("+++++++++++++++++ ComplementAddress = "+ ComplementAddress)
		Thread.sleep(2000)
		WebUI.verifyMatch(GlobalVariable.CompAdresse, ComplementAddress, false, FailureHandling.STOP_ON_FAILURE)
	}
}