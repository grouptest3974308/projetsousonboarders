import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import internal.GlobalVariable
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import groovy.json.JsonSlurper as JsonSlurper

//Added 17/09/2021
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW


class SOUS_10_Add_PhoneNumber {
	//@ValidNum
	//  Scenario: L'utilisateur renseigne un numéro de téléphone valide
	@When('''l'utilisateur renseigne l'indicatif (.*)''')
	def addIndicatif() {
		if(GlobalVariable.indicatif != "France"){
			WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/page_signing_phone'),4)
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/page_signing_phone'),4)
			WebUI.click(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/bouton_indicatif'))
			Thread.sleep(4000)
			WebUI.setText(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/input_indicatif'), GlobalVariable.indicatif)
			WebUI.sendKeys(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/input_indicatif'), Keys.chord(Keys.DOWN))
			WebUI.sendKeys(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/input_indicatif'), Keys.chord(Keys.ENTER))
		}
	}
	@And('''l'utilisateur renseigne son numéro de téléphone''')
	def addNum() {
		//	WebUI.click(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/div_click pour ajouter numero'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/input_phoneNumber'),4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/input_phoneNumber'),4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/input_phoneNumber'), GlobalVariable.numTel)
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/button_Suivant'), 4)
	}
	@And('''l'utilisateur valide le numéro en cliquant sur "Suivant"''')
	def clickOnNextSMS() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/button_Suivant'),4)
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		Thread.sleep(4000)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/4_SMS/2_SMS_Page/page_signing_sms-challenge'), 4)
	}
	@Then('''l'utilisateur reçoit un SMS et la page SMS s'affiche''')
	def codePageDisp() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/4_SMS/2_SMS_Page/page_signing_sms-challenge'),4)
	}

	//@InValidNum
	//Scenario: L'utilisateur renseigne un numéro de téléphone invalide
	@And('''l'utilisateur renseigne un numéro de téléphone invalide''')
	def setNewInValidNum() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/input_phoneNumber'), findTestData('Invalid_personal_data').getValue(10, 11))
		WebUI.click(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/page_signing_phone'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/errorMsg_InvalidNum'),4)
	}
	@Then('''"Numéro de téléphone invalide" s'affiche''')
	def errorMsgInvalidNum() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/errorMsg_InvalidNum'),4)
	}

	//@UpdateNum
	//Scenario: L'utilisateur modifie son numéro de téléphone
	@When('''l'utilisateur clique sur "Modifier le numéro de mobile"''')
	def clickOnUpdateNum() {
		WebUI.click(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/button_UpdateNum'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/page_signing_phone'),4)
	}
	@When('''l'utilisateur renseigne un autre numéro de téléphone''')
	def setNewValidNum() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/4_SMS/1_Phone_Page/input_phoneNumber'), findTestData('ID/Retour_OCR_ID').getValue(11, 1))
		//Mise à jour du numéro de téléphone pour la réception du SMS Challenge après
		GlobalVariable.numTel = findTestData('ID/Retour_OCR_ID').getValue(11, 1)
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/button_Suivant'), 4)

	}
	//@RenvoieSMSChallenge
	//Scenario: L'utilisateur souhaite renvoyer le SMS Challenge

	@When('''l'utilisateur clique sur "Me renvoyer un code SMS"''')
	def sendSMSBack() {
		WebUI.click(findTestObject('Object Repository/SpyElements/4_SMS/2_SMS_Page/button_Renvoyer_SMS'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/4_SMS/2_SMS_Page/page_signing_sms-challenge'),4)

	}
}