import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added on 22/10 for performance test
import java.util.concurrent.TimeUnit
import org.apache.commons.lang3.time.StopWatch


class SOUS_00_Code_Parrainage {
	//Background
	@When('''l'utilisateur clique sur "Ajouter un code de parrainage"''')
	def clickOnAddCodeParrainage() {
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/button_addCodeParrainage'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/input_codeParrainage'), 4)
	}
	//@CP_Code11CaractèresAlphanum
	//Scenario: L'utilisateur renseigne un code de parrainage valide
	@When('''l'utilisateur ajoute un code valide''')
	def addValidCode() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/input_codeParrainage'),findTestData('Invalid_personal_data').getValue(11, 1))
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/button_okCodeParrainage'), 4)
	}
	@And('''l'utilisateur clique sur OK''')
	def clickOnOKCodeParrainage() {
		WebUI.click(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/button_okCodeParrainage'))
	}
	@Then('''le message "validMsg_codeParrainage" s'affiche''')
	def valisMsgDisp() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/validMsg_codeParrainage'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/validMsg_codeParrainage'), 4)
	}
	//#Testcode111
	//@CNP_CodePlusDe11caractères
	// Scenario: L'utilisateur renseigne un code de parrainage invalide contenant plus de 11 caractères
	@When('''l'utilisateur ajoute un code invalide contenant plus de 11 caractères''')
	def addInvalidCode1() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/input_codeParrainage'),findTestData('Invalid_personal_data').getValue(11, 2))
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/button_okCodeParrainage'), 4)
	}
	@Then('''le message d'erreur "errorMsg_codeTropLong" s'affiche''')
	def errorMsg1Disp() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/errorMsg_codeTropLong'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/errorMsg_codeTropLong'), 4)
	}
	//#1234567891011
	//@CNP_CodeMoinsDe11caractères
	//Scenario: L'utilisateur renseigne un code de parrainage invalide contenant moins de 11 caractères
	@When('''l'utilisateur ajoute un code invalide contenant moins de 11 caractères''')
	def addInvalidCode2() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/input_codeParrainage'),findTestData('Invalid_personal_data').getValue(11, 3))
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/button_okCodeParrainage'), 4)
	}
	@Then('''le message d'erreur "errorMsg_codeTropCourt" s'affiche''')
	def errorMsg2Disp() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/errorMsg_codeTropCourt'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/errorMsg_codeTropCourt'), 4)
	}
	//#1234
	//@CNP_CodeAvecCaractèresAlphanum
	//Scenario: L'utilisateur renseigne un code de parrainage invalide contenant un caractères non alphanum
	@When('''l'utilisateur ajoute un code invalide contenant un caractères non alphanum''')
	def addInvalidCode3() {
		WebUI.setText(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/input_codeParrainage'),findTestData('Invalid_personal_data').getValue(11, 4))
	}
	@Then('''le message d'erreur "errorMsg_codeIncorrect" s'affiche''')
	def errorMsg3Disp() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/errorMsg_codeIncorrect'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/1_Page_Registration/00_Code_parrainage/errorMsg_codeIncorrect'), 4)
	}
	//#Testcode11!























}