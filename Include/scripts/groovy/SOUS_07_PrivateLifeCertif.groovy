import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SOUS_07_PrivateLifeCertif {

	@Then('''la page private-life-certification s'affiche''')
	def pageCertif() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/05_Confirm_Info/page_private-life-certification'), 4)
	}

	//@ValidPrivateLifeCertif
	//Scenario: l'utilisateur valide la page private-life-certification
	@When('''l'utilisateur clique sur certification''')
	def clickOnCertif() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/05_Confirm_Info/certification'))

	}
	@And('''l'utilisateur clique sur suivant _Citoyen US''')
	def clickOnNextUS() {
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/1_BankAccountType/page_account-type'), 4)

	}
	@Then('''la page account-type s'affiche''')
	def accountPageDisp() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/1_BankAccountType/page_account-type'), 4)

	}


	// @Update_RF_PrivateLifeCertif
	//Scenario: Vérification du bouton "Modifier"_Résidence fiscale_Page private-life-certification
	@When('''l'utilisateur clique sur modifier_RF''')
	def updateButtonRF() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/05_Confirm_Info/button_update_RF'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/03_Résidence_fisc/page_tax-residence'), 4)
	}

	//@Update_CitoyenUS_PrivateLifeCertif
	//Scenario: Vérification du bouton "Modifier"_CitoyenUS_Page private-life-certification
	@When('''l'utilisateur clique sur modifier_CitoyenUS''')
	def updateButtonCitoyenUS() {
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/05_Confirm_Info/button_update_CitoyenUS'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/04_Citoyen_US/page_us-person'), 4)

	}
}