import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys

// Added on 25/10/2021
import java.time.*
import java.text.SimpleDateFormat

class SOUS_08_InfoPerso {
	//Background:
	@Given('''la page civility-confirmation s'affiche''')
	def checkPageCiv() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/page_civility-confirmation'), 4)
	}

	//@ValidCiviliyConfirmation
	//Scenario: Validation des informations personnelles
	@When('''l'utilisateur s'assure que ses données sont bien préremplis''')
	def checkData() {
		String nom = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_nom'), 'value')
		String prenom = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_prenom'), 'value')

		//Récupération du Retour OCR dans les variables prenom_OCR & nom_OCR afin de les comparer au champs nom et prénom de l'interface Web
		switch (GlobalVariable.typeOfID) {
			case "Passeport":
			//Vérification que la ligne de la basede données Retour_OCR_ID correspond bien au même type d'ID
				WebUI.verifyMatch(findTestData('ID/Retour_OCR_ID').getValue(1, 2), GlobalVariable.typeOfID, false, FailureHandling.STOP_ON_FAILURE)
				String prenom_OCR= findTestData('ID/Retour_OCR_ID').getValue(4, 2)
				String nom_OCR= findTestData('ID/Retour_OCR_ID').getValue(5, 2)
				WebUI.verifyMatch(nom, nom_OCR, false, FailureHandling.STOP_ON_FAILURE)
				WebUI.verifyMatch(prenom, prenom_OCR, false, FailureHandling.STOP_ON_FAILURE)
			//@ValidCiviliyConfirmation: Mise à jour de la variable globale avec le retour OCR
			//@UpdateCiviliyConfirmation: Ne pas faire  la mise à jour
			// => La mise à jour est obligatoire car GlobalVariable.firstName sont utilisés pour la partie Challenge SMS
				GlobalVariable.firstName=prenom_OCR
				GlobalVariable.lastName=nom_OCR
				break
			case "Carte d'identité":
				WebUI.verifyMatch(findTestData('ID/Retour_OCR_ID').getValue(1, 1), GlobalVariable.typeOfID, false, FailureHandling.STOP_ON_FAILURE)
				String prenom_OCR= findTestData('ID/Retour_OCR_ID').getValue(4, 1)
				String nom_OCR= findTestData('ID/Retour_OCR_ID').getValue(5, 1)
				WebUI.verifyMatch(nom, nom_OCR, false, FailureHandling.STOP_ON_FAILURE)
				WebUI.verifyMatch(prenom, prenom_OCR, false, FailureHandling.STOP_ON_FAILURE)
				GlobalVariable.firstName=prenom_OCR
				GlobalVariable.lastName=nom_OCR
				break
			case "Titre de séjour":
				WebUI.verifyMatch(findTestData('ID/Retour_OCR_ID').getValue(1, 3), GlobalVariable.typeOfID, false, FailureHandling.STOP_ON_FAILURE)
				String prenom_OCR= findTestData('ID/Retour_OCR_ID').getValue(4, 3)
				String nom_OCR= findTestData('ID/Retour_OCR_ID').getValue(5, 3)
				WebUI.verifyMatch(nom, nom_OCR, false, FailureHandling.STOP_ON_FAILURE)
				WebUI.verifyMatch(prenom, prenom_OCR, false, FailureHandling.STOP_ON_FAILURE)
				GlobalVariable.firstName=prenom_OCR
				GlobalVariable.lastName=nom_OCR
				break
		}
		println ("+++++++++++++++++++++ First name :"+ GlobalVariable.firstName + "++++++++++++++++++++++++++++")
		println ("+++++++++++++++++++++ Last name :"+ GlobalVariable.lastName+ "++++++++++++++++++++++++++++")
	}
	@And('''l'utilisateur clique sur "Suivant" _KYC''')
	def clickOnNext1() {
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'), 4)
	}
	@Then('''la page birth-data-confirmation s'affiche _KYC''')
	def checkPageBirth() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'), 4)
	}
	@When('''l'utilisateur s'assure que ses données de naissance sont bien préremplis''')
	def checkBirthData() {
		String dateDeNaissance = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), 'value')
		String nationalite = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_nationalit'), 'value')
		//Récupération du Retour OCR dans les variables prenom_OCR & nom_OCR afin de les comparer au champs nom et prénom de l'interface Web
		switch (GlobalVariable.typeOfID) {
			case "Passeport":
			//Vérification que la ligne de la basede données Retour_OCR_ID correspond bien au même type d'ID
				WebUI.verifyMatch(findTestData('ID/Retour_OCR_ID').getValue(1, 2), GlobalVariable.typeOfID, false, FailureHandling.STOP_ON_FAILURE)
				String dateOfBirth_OCR= findTestData('ID/Retour_OCR_ID').getValue(6, 2)
				String nationality_OCR= findTestData('ID/Retour_OCR_ID').getValue(7, 2)
				WebUI.verifyMatch(dateDeNaissance, dateOfBirth_OCR, false, FailureHandling.STOP_ON_FAILURE)
				WebUI.verifyMatch(nationalite, nationality_OCR, false, FailureHandling.STOP_ON_FAILURE)
				GlobalVariable.dateOfBirth =dateOfBirth_OCR
				GlobalVariable.nationality = nationality_OCR
				break
			case "Carte d'identité":
				WebUI.verifyMatch(findTestData('ID/Retour_OCR_ID').getValue(1, 1), GlobalVariable.typeOfID, false, FailureHandling.STOP_ON_FAILURE)
				String dateOfBirth_OCR= findTestData('ID/Retour_OCR_ID').getValue(6, 1)
				String nationality_OCR= findTestData('ID/Retour_OCR_ID').getValue(7, 1)
				WebUI.verifyMatch(dateDeNaissance, dateOfBirth_OCR, false, FailureHandling.STOP_ON_FAILURE)
				WebUI.verifyMatch(nationalite, nationality_OCR, false, FailureHandling.STOP_ON_FAILURE)
				GlobalVariable.dateOfBirth =dateOfBirth_OCR
				GlobalVariable.nationality = nationality_OCR
				break
			case "Titre de séjour":
				WebUI.verifyMatch(findTestData('ID/Retour_OCR_ID').getValue(1, 3), GlobalVariable.typeOfID, false, FailureHandling.STOP_ON_FAILURE)
				String dateOfBirth_OCR= findTestData('ID/Retour_OCR_ID').getValue(6, 3)
				String nationality_OCR= findTestData('ID/Retour_OCR_ID').getValue(7, 3)
				WebUI.verifyMatch(dateDeNaissance, dateOfBirth_OCR, false, FailureHandling.STOP_ON_FAILURE)
				WebUI.verifyMatch(nationalite, nationality_OCR, false, FailureHandling.STOP_ON_FAILURE)
				GlobalVariable.dateOfBirth =dateOfBirth_OCR
				GlobalVariable.nationality = nationality_OCR
				break
		}}

	@And('''l'utilisateur renseigne son pays de naissance''')
	def addPays() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_pays'), 4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_pays'), GlobalVariable.nativeCountry)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'))
		if (GlobalVariable.nativeCountry != GlobalVariable.nationality ){
			WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/popup_nationality different from nativeCountry'),4)
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/popup_nationality different from nativeCountry'),4)
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/button_JeSuisSur'))
		}
		//WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'))
		if ((GlobalVariable.nativeCountry=="France") || (GlobalVariable.nativeCountry=="Espagne")){
			WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_departement'),4)
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_departement'),4)
			WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_departement'),GlobalVariable.departmentOfBirth)
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'))
		}
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_ville'),4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_ville'),GlobalVariable.cityOfBirth)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'))
	}

	@And('''l'utilisateur clique sur "Suivant" _KYC2''')
	def clickOnNext2() {
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/3_Page_CGD/page_signing_terms-and-conditions'), 4)
	}


	//@UpdateCiviliyConfirmation
	//Scenario: Mise à jour des informations personnelles
	@When('''l'utilisateur met à jour son staut''')
	def updateStatut() {
		if (findTestData('ID/Retour_OCR_ID').getValue(3, 1)== "Monsieur") {
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/button_Mme'),4)
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/button_Mme'))
			Thread.sleep(2000)
			GlobalVariable.statut="Madame"
		} else if (findTestData('ID/Retour_OCR_ID').getValue(3, 1)== "Madame") {
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/button_Mr'),4)
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/button_Mr'));
			Thread.sleep(2000)
			GlobalVariable.statut="Monsieur"
		}
	}
	@And('''l'utilisateur met à jour son nom''')
	def enterNom() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_nom'), 4)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_nom'), Keys.chord(Keys.CLEAR))
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_nom'), GlobalVariable.lastName )

	}

	@And('''l'utilisateur met à jour son prénom''')
	def enterPrenom() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_prenom'),4)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_prenom'), Keys.chord(Keys.CLEAR))
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_prenom'),GlobalVariable.firstName)
	}
	@And('''l'utilisateur met à jour son nom marital''')
	def enterNomMarital() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_nom marital'), 4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_nom marital'), GlobalVariable.nomMarital )
	}

	@When('''l'utilisateur met à jour sa date de naissance''')
	def enterDateNaissance() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'),4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'),GlobalVariable.dateOfBirth)
	}

	@And('''l'utilisateur met à jour sa nationalité''')
	def addNationalit() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_nationalit'),4)
		//WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/button_nationalit'))
		//Thread.sleep(2000)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_nationalit'), Keys.chord(Keys.CLEAR))
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_nationalit'), GlobalVariable.nationality)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_nationalit'), Keys.chord(Keys.DOWN))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_nationalit'), Keys.chord(Keys.ENTER))

		if (GlobalVariable.nativeCountry != GlobalVariable.nationality) {
			WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/popup_nationality different from nativeCountry'), 4)
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/popup_nationality different from nativeCountry'), 4)
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/button_JeSuisSur'),4)
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/button_JeSuisSur'));
		}
	}


	//#Cas non passant
	//@FirstNameContainingSpecialCaracter
	//Scenario: Le pénom contient un caractère spécial (cas non passant)
	@When('''l'utilisateur renseigne un prénom avec un caractère spécial''')
	def wrongFirstName() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_prenom'),4)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_prenom'), Keys.chord(Keys.CLEAR))
		Thread.sleep(3000)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_prenom'),findTestData('Invalid_personal_data').getValue(2, 4))
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/page_civility-confirmation'))
	}
	@Then('''le message d'erreur 3 s'affiche''')
	def errorMessage3() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_wrongFirstName'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_wrongFirstName'), 4)
	}
	//#Cas non passant
	//@LastNameContainingSpecialCaracter
	//Scenario: Le nom contient un caractère spécial (cas non passant)
	@When('''l'utilisateur renseigne un nom avec un caractère spécial''')
	def wrongLastName() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_nom'),4)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_nom'), Keys.chord(Keys.CLEAR))
		Thread.sleep(3000)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/input_nom'),findTestData('Invalid_personal_data').getValue(3, 5))
		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/page_civility-confirmation'))
	}
	@Then('''le message d'erreur 4 s'affiche''')
	def errorMessage4() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_wrongLastName'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_wrongLastName'), 4)
	}
	//#Cas non passant
	//@Age_above_120
	//Scenario: Utilisateur a plus de 120 ans (cas non passant)
	@When('''l'utilisateur renseigne une date de naissance erronnée >120ans''')
	def ageAbove120(){
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), 4)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), Keys.chord(Keys.CONTROL,'a'))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), Keys.chord(Keys.BACK_SPACE))

		Thread.sleep(3000)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'),findTestData('Invalid_personal_data').getValue(5, 7))
		Thread.sleep(2000)
		String birthDate = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), 'value')
		println("Date de naissance :"+ birthDate)

		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'))
	}
	@Then('''le message d'erreur 1 s'affiche''')
	def errorMessage1() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_ageAbove120'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_ageAbove120'), 4)
	}


	//@Age_below_18
	//Scenario: Utilisateur a moins de 18 ans (cas non passant)
	@When('''l'utilisateur renseigne une date de naissance erronnée <18ans''')
	def ageBelow18() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), 4)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), Keys.chord(Keys.CONTROL,'a'))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), Keys.chord(Keys.BACK_SPACE))
		Thread.sleep(3000)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'),findTestData('Invalid_personal_data').getValue(5, 6))
		Thread.sleep(2000)
		String birthDate = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), 'value')
		println("Date de naissance :"+ birthDate)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'))
	}
	@Then('''le message d'erreur 2 s'affiche''')
	def errorMessage2() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_ageBelow18'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_ageBelow18'), 4)
	}

	//@Age_limite_120
	//Scenario: Utilisateur a plus de 120 ans (cas aux limites)
	@When('''l'utilisateur renseigne une date de naissance limite correspondant à 120 ans''')
	def ageLimit120(){
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'),4)
		LocalDate today    = LocalDate.now()
		//Définition de la date de naissance limite correspondant à 120ans
		String yyyy_limit_120= today.year - 120
		String mm = today.monthValue
		String dd = today.dayOfMonth

		if (today.monthValue<10) {
			mm = "0"+ today.monthValue
		}
		if (today.dayOfMonth<10) {
			dd = "0"+ today.dayOfMonth}

		String birthDate_limit_120 = WebUI.concatenate([
			dd,
			"/",
			mm,
			"/",
			yyyy_limit_120] as String[], FailureHandling.STOP_ON_FAILURE)

		println(birthDate_limit_120)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), 4)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), Keys.chord(Keys.CONTROL,'a'))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), Keys.chord(Keys.BACK_SPACE))
		Thread.sleep(3000)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'),birthDate_limit_120)
		String birthDate = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), 'value')
		println("Date de naissance :"+ birthDate)

		Thread.sleep(2000)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'))
	}
	@Then('''le message d'erreur 1 ne s'affiche pas''')
	def errorMessage1NotDisp() {
		WebUI.waitForElementNotVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_ageAbove120'), 4)
		WebUI.verifyElementNotPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_ageAbove120'), 4)
	}

	//@Age_limite_18
	//Scenario: Utilisateur a moins de 18 ans (cas aux limites)
	@When('''l'utilisateur renseigne une date de naissance limite correspondant à 18 ans''')
	def ageLimit18() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'),4)
		LocalDate today    = LocalDate.now()
		//Définition de la date de naissance limite correspondant à 18ans
		String yyyy_limit_18= today.year -18
		String mm = today.monthValue
		String dd = today.dayOfMonth

		if (today.monthValue<10) {
			mm = "0"+ today.monthValue
		}
		if (today.dayOfMonth<10) {
			dd = "0"+ today.dayOfMonth}
		String birthDate_limit_18  = WebUI.concatenate([
			dd,
			"/",
			mm,
			"/",
			yyyy_limit_18]  as String[], FailureHandling.STOP_ON_FAILURE)

		println(birthDate_limit_18)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), 4)
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), Keys.chord(Keys.CONTROL,'a'))
		WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), Keys.chord(Keys.BACK_SPACE))
		Thread.sleep(3000)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'),birthDate_limit_18)
		Thread.sleep(2000)
		String birthDate = WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/input_dateDeNaissance'), 'value')
		println("Date de naissance :"+ birthDate)

		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/4_birth-data-confirmation/page_birth-data-confirmation'))
	}
	@Then('''le message d'erreur 2 ne s'affiche pas''')
	def errorMessage2notDisp() {
		WebUI.waitForElementNotVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_ageBelow18'), 4)
		WebUI.verifyElementNotPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/errorMsg_ageBelow18'), 4)
	}



}
