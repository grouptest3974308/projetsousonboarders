Feature: Vérifier que l’utilisateur peut renseigner son numéro de télephone et recevoir le challenge SMS

  @ValidNum
  Scenario: L'utilisateur renseigne un numéro de téléphone valide
    Given la page mobile "Tapez votre numéro de mobile" s'affiche
    #When l'utilisateur renseigne l'indicatif <Indicatif>
    And l'utilisateur renseigne son numéro de téléphone
    And l'utilisateur valide le numéro en cliquant sur "Suivant"
    Then l'utilisateur reçoit un SMS et la page SMS s'affiche

  @InValidNum
  Scenario: L'utilisateur renseigne un numéro de téléphone invalide
    Given la page mobile "Tapez votre numéro de mobile" s'affiche
    #When l'utilisateur renseigne l'indicatif <Indicatif>
    And l'utilisateur renseigne un numéro de téléphone invalide
    Then "Numéro de téléphone invalide" s'affiche

  @UpdateNum
  Scenario: L'utilisateur modifie son numéro de téléphone
    Given la page mobile "Tapez votre numéro de mobile" s'affiche
    #When l'utilisateur renseigne l'indicatif <Indicatif>
    And l'utilisateur renseigne son numéro de téléphone
    And l'utilisateur valide le numéro en cliquant sur "Suivant"
    Then l'utilisateur reçoit un SMS et la page SMS s'affiche
    When l'utilisateur clique sur "Modifier le numéro de mobile"
    Then la page mobile "Tapez votre numéro de mobile" s'affiche
    #When l'utilisateur renseigne l'indicatif <Indicatif>
    When l'utilisateur renseigne un autre numéro de téléphone
    And l'utilisateur valide le numéro en cliquant sur "Suivant"
    Then l'utilisateur reçoit un SMS et la page SMS s'affiche

  @RenvoieSMSChallenge
  Scenario: L'utilisateur souhaite renvoyer le SMS Challenge
    Given la page mobile "Tapez votre numéro de mobile" s'affiche
    #When l'utilisateur renseigne l'indicatif <Indicatif>
    And l'utilisateur renseigne son numéro de téléphone
    And l'utilisateur valide le numéro en cliquant sur "Suivant"
    Then l'utilisateur reçoit un SMS et la page SMS s'affiche
    When l'utilisateur clique sur "Me renvoyer un code SMS"
    Then l'utilisateur reçoit un SMS et la page SMS s'affiche
