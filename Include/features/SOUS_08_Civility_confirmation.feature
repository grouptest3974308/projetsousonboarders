Feature: Vérifier que l'utilisateur peut mettre à jour ses informations personnelles

  Background: 
    Given la page civility-confirmation s'affiche

  @ValidCivilityInformation
  Scenario: Validation des informations personnelles
    When l'utilisateur s'assure que ses données sont bien préremplis
    And l'utilisateur clique sur "Suivant" _KYC
    Then la page birth-data-confirmation s'affiche _KYC
    When l'utilisateur s'assure que ses données de naissance sont bien préremplis
    And l'utilisateur renseigne son pays de naissance
    And l'utilisateur clique sur "Suivant" _KYC2
    Then la page des conditions générales "CDG" s'affiche

  @UpdateCivilityInformation
  Scenario: Mise à jour des informations personnelles
    When l'utilisateur met à jour son staut
    And l'utilisateur met à jour son nom
    And l'utilisateur met à jour son prénom
    And l'utilisateur met à jour son nom marital
    And l'utilisateur clique sur "Suivant" _KYC
    Then la page birth-data-confirmation s'affiche _KYC
    When l'utilisateur met à jour sa date de naissance
    And l'utilisateur renseigne son pays de naissance
    And l'utilisateur met à jour sa nationalité
    And l'utilisateur clique sur "Suivant" _KYC2
    Then la page des conditions générales "CDG" s'affiche

  #Cas non passant
  @FirstNameContainingSpecialCaracter
  Scenario: Le pénom contient un caractère spécial (cas non passant)
    When l'utilisateur renseigne un prénom avec un caractère spécial
    Then le message d'erreur 3 s'affiche

  @LastNameContainingSpecialCaracter
  Scenario: Le nom contient un caractère spécial (cas non passant)
    When l'utilisateur renseigne un nom avec un caractère spécial
    Then le message d'erreur 4 s'affiche

  @Age_above_120
  Scenario: Utilisateur a plus de 120 ans (cas non passant)
    When l'utilisateur s'assure que ses données sont bien préremplis
    And l'utilisateur clique sur "Suivant" _KYC
    Then la page birth-data-confirmation s'affiche _KYC
    When l'utilisateur renseigne une date de naissance erronnée >120ans
    Then le message d'erreur 1 s'affiche

  @Age_below_18
  Scenario: Utilisateur a moins de 18 ans (cas non passant)
    When l'utilisateur s'assure que ses données sont bien préremplis
    And l'utilisateur clique sur "Suivant" _KYC
    Then la page birth-data-confirmation s'affiche _KYC
    When l'utilisateur renseigne une date de naissance erronnée <18ans
    Then le message d'erreur 2 s'affiche

  @Age_limite_120
  Scenario: Utilisateur a plus de 120 ans (cas aux limites)
    When l'utilisateur s'assure que ses données sont bien préremplis
    And l'utilisateur clique sur "Suivant" _KYC
    Then la page birth-data-confirmation s'affiche _KYC
    When l'utilisateur renseigne une date de naissance limite correspondant à 120 ans
    Then le message d'erreur 1 ne s'affiche pas

  @Age_limite_18
  Scenario: Utilisateur a moins de 18 ans (cas aux limites)
    When l'utilisateur s'assure que ses données sont bien préremplis
    And l'utilisateur clique sur "Suivant" _KYC
    Then la page birth-data-confirmation s'affiche _KYC
    When l'utilisateur renseigne une date de naissance limite correspondant à 18 ans
    Then le message d'erreur 2 ne s'affiche pas
