Feature: Vérifier qu'il est possible d'accepter, refuser ou fermer le consentement biométrique

  Background: Vérifier qu'il est possible d'accepter, refuser ou fermer le consentement biométrique
    Given le pop-up "Consentement lié à la comparaison entre votre selfie et votre pièce d’identité" s'affiche

  @AcceptConsent
  Scenario: #1 Vérifier qu'il est possible de passer à l'étape suivante en acceptant le consentement biométrique
    When l'utilisateur clique sur le bouton "Accepter et continuer"
    Then la page "Quelle est votre situation familiale ?" s'affiche
    #And à la validation de la souscription, le selfie est analysé dans Onfido
  
  @DenyConsent
  Scenario: #2 vérifier qu'il est possible de passer à l'étape suivante en refusant le consentement biométrique
    When l'utilisateur clique sur le bouton "Continuer sans accepter"
    Then la page "Quelle est votre situation familiale ?" s'affiche
    # And à la validation de la souscription, le selfie n'est pas analysé dans Onfido
  
  @CloseConsent
  Scenario: #3 Vérifier qu'il est possible de passer à l'étape suivante en fermant le consentement biométrique
    When l'utilisateur clique sur la "Croix"
    Then le pop-up disparait
    And la page "Quelle est votre situation familiale ?" s'affiche
    #And à la validation de la souscription, le selfie n'est pas analysé dans Onfido