Feature: Vérifier que l'utilisateur peut signer les CDG

  Background: Siganture des conditions générales et tarifaires
    Given la page des conditions générales "CDG" s'affiche
    And la vérification des contrats est OK

  @VerifCDG_NePasAccepter
  Scenario: #1
    When l'utilisateur signe le contrat
    Then la page mobile "Tapez votre numéro de mobile" s'affiche

  @VerifCDG_Nickel
  Scenario: #2
    When l'utilisateur clique sur "Accepter que les données soient utilisées par Nickel"
    When l'utilisateur signe le contrat
    Then la page mobile "Tapez votre numéro de mobile" s'affiche

  @VerifCDG_Partners
  Scenario: #3
    When l'utilisateur clique sur "Accepter que les données soient utilisées par les partenaires"
    When l'utilisateur signe le contrat
    Then la page mobile "Tapez votre numéro de mobile" s'affiche

  @VerifCDG_Nickel&Partners
  Scenario: #4
    When l'utilisateur clique sur "Accepter que les données soient utilisées par Nickel"
    And l'utilisateur clique sur "Accepter que les données soient utilisées par les partenaires"
    When l'utilisateur signe le contrat
    Then la page mobile "Tapez votre numéro de mobile" s'affiche
