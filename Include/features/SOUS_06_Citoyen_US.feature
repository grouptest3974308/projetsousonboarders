Feature: Vérification citoyen US

  Background: 
    Given l'utilisateur est sur la page Citoyen US

  @NonCitoyenUS
  Scenario: l'utilisateur n'est pas citoyen américain
    When l'utilisateur clique sur "Non" _US
    Then la page private-life-certification s'affiche

  @CitoyenUSETRésident_BackToForm
  Scenario: l'utilisateur est citoyen et résident américain
    When l'utilisateur clique sur "Oui" _US
    And l'utilisateur clique sur "Citoyen ou résident"
    Then la popup "Client non accepté" s'affiche
    Then l'utilisateur clique sur "Revenir au formulaire"
    And l'utilisateur clique sur "Né et non citoyen" _US
    Then la popup de document à fournir "Document à fournir" s'affiche
    And l'utilisateur clique sur "J'ai compris" _MAJSituation
    Then la page private-life-certification s'affiche

  @CitoyenUSETRésident_Cancel
  Scenario: l'utilisateur est citoyen et résident américain => Annnulation de l'ouverture du compte
    When l'utilisateur clique sur "Oui" _US
    And l'utilisateur clique sur "Citoyen ou résident"
    Then la popup "Client non accepté" s'affiche
    Then l'utilisateur annule l'ouverture de son compte en cliquant sur "Annuler l'ouverture de mon compte"