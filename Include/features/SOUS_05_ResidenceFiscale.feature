Feature: Vérifier que l'utilisateur peut ajouter une résidence fiscale s'il le souhaite

  Background: 
    Given la page tax_residence s'affiche

  @RésidenceFiscaleUniqueFr
  Scenario: #1
    When l'utilisateur clique sur Oui
    Then la page citoyen US s'affiche

  @AjoutRésidenceFiscale2
  Scenario: #2
    When l'utilisateur clique sur Non
    Then la popup NumIdFisc s'affiche
    When l'utilisateur clique sur J'ai compris
    And l'utilisateur renseigne une résidence fiscale UE_AELE
    And l'utilisateur clique sur suivant
    Then la page citoyen US s'affiche

  @SupprimerRésidenceFiscale2
  Scenario: #3
    When l'utilisateur clique sur Non
    Then la popup NumIdFisc s'affiche
    When l'utilisateur clique sur J'ai compris
    And l'utilisateur renseigne une résidence fiscale UE_AELE
    When l'utilisateur supprime sa deuxième résidence fiscale
    And l'utilisateur clique sur suivant
    Then la popup Résidences fiscales s'affiche
    When l'utilisateur clique sur Résidence fisc uniquement en Fr
    Then la page citoyen US s'affiche

  @RésidenceFiscaleHors_UE_AELE
  Scenario: #4
    When l'utilisateur clique sur Non
    Then la popup NumIdFisc s'affiche
    When l'utilisateur clique sur J'ai compris
   #And l'utilisateur supprime sa première résidence fiscale
    And l'utilisateur renseigne une résidence fiscale hors UE_AELE
    Then le message "Nickel n’est pas disponible pour les clients ayant des résidences fiscales hors UE ou AELE" s'affiche
    And le bouton suivant est grisé
    When l'utilisateur supprime sa deuxième résidence fiscale
  # And l'utilisateur renseigne une résidence fiscale UE_AELE
    When l'utilisateur clique sur suivant
		Then la popup Résidences fiscales s'affiche
    When l'utilisateur clique sur Résidence fisc uniquement en Fr
    Then la page citoyen US s'affiche

  @RésidenceFiscaleHors_UE_AELE_MSC
  Scenario: #5
    When l'utilisateur clique sur Non
    Then la popup NumIdFisc s'affiche
    When l'utilisateur clique sur J'ai compris
   # And l'utilisateur supprime sa première résidence fiscale
    And l'utilisateur renseigne une résidence fiscale hors UE_AELE_MSC
    Then le message "Nickel n’est pas disponible pour les clients ayant des résidences fiscales hors UE ou AELE" s'affiche
    And le bouton suivant est grisé
    When l'utilisateur supprime sa deuxième résidence fiscale
   #And l'utilisateur renseigne une résidence fiscale UE_AELE
    When l'utilisateur clique sur suivant
    Then la popup Résidences fiscales s'affiche
    When l'utilisateur clique sur Résidence fisc uniquement en Fr
    Then la page citoyen US s'affiche

  @RésidenceFiscale_UE_AELE_Only
  Scenario: #6
    When l'utilisateur clique sur Non
    Then la popup NumIdFisc s'affiche
    When l'utilisateur clique sur J'ai compris
    And l'utilisateur supprime sa première résidence fiscale
    And l'utilisateur renseigne une résidence fiscale UE_AELE
    And l'utilisateur clique sur suivant
    Then la page citoyen US s'affiche
    
  @SupprimerRF2_RevenirAuFormulaire
  Scenario: #7
    When l'utilisateur clique sur Non
    Then la popup NumIdFisc s'affiche
    When l'utilisateur clique sur J'ai compris
    And l'utilisateur renseigne une résidence fiscale UE_AELE
    When l'utilisateur supprime sa deuxième résidence fiscale
    And l'utilisateur clique sur suivant
    Then la popup Résidences fiscales s'affiche
    When l'utilisateur clique sur Revenir au formulaire
    Then la page tax_residence s'affiche
    And l'utilisateur clique sur suivant
    Then la popup Résidences fiscales s'affiche
    When l'utilisateur clique sur Résidence fisc uniquement en Fr
    Then la page citoyen US s'affiche
