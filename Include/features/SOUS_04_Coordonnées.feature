Feature: Vérifier que l'utilisateur peut renseigner son adresse manuellement ou automatiquement

  Background: 
    Given la page "Saisissez votre adresse postale" s'affiche

  @AddGoogAdd
  Scenario: Saisie via Google Adresse
    When l'utlisateur saisi son adresse
    And l'utilisateur renseigne complément d'adresse
    When l'utilisateur clique sur "Suivant"
    Then la page tax_residence s'affiche

  @AddManAdd
  Scenario: Saisie manuelle
    When l'utilisateur clique sur "saisir manuellement"
    When l'utilisateur renseigne code postal
    And l'utilisateur renseigne ville
    And l'utilisateur renseigne numéro de rue
    And l'utilisateur renseigne nom de rue
    And l'utilisateur renseigne complément d'adresse
    When l'utilisateur clique sur "Suivant"
    Then la page tax_residence s'affiche

  @AddWithoutNumRue_MoreThan32Character
  Scenario: Saisie manuelle_Sans Numéro de rue_Nom de rue et complément d'adresse contennant plus de 32 caractères
    When l'utilisateur clique sur "saisir manuellement"
    When l'utilisateur renseigne code postal
    And l'utilisateur renseigne ville
    And l'utilisateur renseigne nom de rue contenant plus de 32 caractères
    And l'utilisateur renseigne complément d'adresse contenant plus de 32 caractères
    When l'utilisateur clique sur "Suivant"
    Then la page tax_residence s'affiche

  @FocusAddressCheck
  Scenario: Vérifier que le focus sur l'adresse ne se perd pas si on clique ailleurs
    When l'utlisateur commence à saisir son adresse et clique ailleurs
    Then le champ adresse ne se vide pas
    When l'utilisateur clique sur "Suivant"
    Then la page tax_residence s'affiche
    
  @ValidationAddressAvecRetour
  Scenario: Vérifier que lorsqu'on saisi une adresse, qu'on valide et qu'on revient sur l'écran adresse postale, la saisie est toujours présente
    When l'utlisateur saisi son adresse
    And l'utilisateur renseigne complément d'adresse
    When l'utilisateur clique sur "Suivant"
    Then la page tax_residence s'affiche
    And l'utilisateur clique sur "Retour"
    Then le champ adresse ne se vide pas
    And le champ Complément d'adresse ne se vide pas
    When l'utilisateur clique sur "Suivant"
    Then la page tax_residence s'affiche
