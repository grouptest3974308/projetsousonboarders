Feature: L’utilisateur doit renseigner son adresse e-mail pour pouvoir poursuivre le parcours de RDC

  Background: 
    Given l'utilisateur sélectionne le pays de souscription et clique sur Suivant
    When l'utilisateur clique sur "Ouvrir un compe en 5 minutes"

  @withoutEmail
  Scenario: l'utilisateur continue sans renseigner une adresse mail
    When l'utilisateur clique sans renseigner le mail sur "Démarrer"
    Then la popup "Recommendation mail" s'affiche
    When l'utilisateur clique sur "Continuer sans renseigner"
    Then la page "Avez-vous déjà eu un compte Nickel ?" s'affiche

  @validEmail
  Scenario: l'utilisateur renseigne une adresse mail valide
    When l'utilisateur renseigne une adresse "e-mail" valide
    And l'utilisateur clique sur "Démarrer"
    Then la page "Avez-vous déjà eu un compte Nickel ?" s'affiche

  @InvalidEmail_hote_inconnu
  Scenario: l'utilisateur renseigne une adresse mail invalide
    When l'utilisateur renseigne une adresse "e-mail" avec hote inconnu
    Then Le message d'erreur "Adresse mail invalide" s'affiche

  @InvalidEmail_structure_invalide
  Scenario: 
    When l'utilisateur renseigne une adresse "e-mail" avec structure invalide
    Then Le message d'erreur "Adresse mail invalide" s'affiche

  @InvalidEmail_utilisateur_inconnu
  Scenario: 
    When l'utilisateur renseigne une adresse "e-mail" avec utilisateur inconnu
    Then Le message d'erreur "Adresse mail invalide" s'affiche

  @InvalidEmail_performance_test
  Scenario: 
    When l'utilisateur renseigne une adresse "e-mail" avec utilisateur inconnu
    Then Le message d'erreur "Adresse mail invalide" s'affiche et l'évaluation du temps d'affichage est établie
