Feature: Vérifier que l'utilisateur peut se prendre en photo avec la possibilité de recommencer s'il le souhaite

  Background:  se prendre en selfie
    Given la page "Prenez un selfie" s'affiche

  @Selfie
  Scenario: #1 Upload AND se prendre en selfie
    When l'utilisateur clique sur "Continuer"
    And l'utilisateur se prend en photo et clique sur "Envoyer"
    #Then la page "Quelle est votre situation familiale ?" s'affiche
		Then le pop-up "Consentement lié à la comparaison entre votre selfie et votre pièce d’identité" s'affiche

  @Restart_Selfie
  Scenario: #2 Upload AND Recommencer un selfie
    When l'utilisateur clique sur "Continuer"
    When l'utilisateur se prend en photo et clique sur "Recommencer"
    And l'utilisateur se prend en photo et clique sur "Envoyer"
    #Then la page "Quelle est votre situation familiale ?" s'affiche
		Then le pop-up "Consentement lié à la comparaison entre votre selfie et votre pièce d’identité" s'affiche
