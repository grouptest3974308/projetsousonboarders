Feature: Vérifier que l’utilisateur peut renseigner le challenge SMS # sur le parcours FR, ES, PT et BE


  Scenario: 
    Given l'utilisateur reçoit un SMS et la page SMS s'affiche
    When l'utilisateur renseigne le challenge SMS
    And l'utilisateur valide le challenge SMS en cliquant sur "Suivant"
    Then la réouverture de compte a été prise en compte
    Then le code à 5 chiffres affiché correspond au code d'activation reçu par SMS
    # Then le code à 5 chiffres affiché correspond au code d'activation reçu par Email