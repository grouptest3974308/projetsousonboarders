Feature: Vérifier que l'utilisateur peut renseigner son code de parrainage

  Background: 
    Given l'utilisateur sélectionne le pays de souscription et clique sur Suivant
   When l'utilisateur clique sur "Ajouter un code de parrainage"

  @CP_CodeValide
  Scenario: L'utilisateur renseigne un code de parrainage valide #Testcode111
  When l'utilisateur ajoute un code valide
  And l'utilisateur clique sur OK
  Then le message "validMsg_codeParrainage" s'affiche

  @CNP_CodePlusDe11caractères
  Scenario: L'utilisateur renseigne un code de parrainage invalide contenant plus de 11 caractères #1234567891011
  When l'utilisateur ajoute un code invalide contenant plus de 11 caractères
  And l'utilisateur clique sur OK
	Then le message d'erreur "errorMsg_codeTropLong" s'affiche

 @CNP_CodeMoinsDe11caractères
  Scenario: L'utilisateur renseigne un code de parrainage invalide contenant moins de 11 caractères   #1234
   When l'utilisateur ajoute un code invalide contenant moins de 11 caractères
   And l'utilisateur clique sur OK
   Then le message d'erreur "errorMsg_codeTropCourt" s'affiche

 @CNP_CodeAvecCaractèresAlphanum
  Scenario: L'utilisateur renseigne un code de parrainage invalide contenant un caractères non alphanum 	#Testcode11!
   When l'utilisateur ajoute un code invalide contenant un caractères non alphanum
   And l'utilisateur clique sur OK
   Then le message d'erreur "errorMsg_codeIncorrect" s'affiche
