Feature: En fonction de la pièce sélectionnée l'utilisateur devra pouvoir télécharger la pièce correspondante

  Background: 
    Given la page "Avez-vous déjà eu un compte Nickel ?" s'affiche
    And l'utilisateur clique sur "Non"
    And l'utilisateur clique sur "Sélectionner un document"
    And l'utilisateur sélectionne sa pièce d'identité
  @validID
  Scenario: SDK Onfido - Upload d'un passeport conforme web - P (SOUS-2550 - SOUS-2625 - SOUS-2625)
    And l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce
    Then la page "Prenez un selfie" s'affiche

  @CP_ID_Forçage
  Scenario: SDK Onfido - Forçage Upload Passeport - P (SOUS-2601)
    And l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce tronquée
    And l'utilisateur clique sur "Recommencer"
    And l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce avec reflet
    And l'utilisateur clique sur "Recommencer"
    And l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce floue
    And l'utilisateur clique sur "Envoyer quand même"
    Then la page "Prenez un selfie" s'affiche

  @CNP_ID_tronqué
  Scenario: SDK Onfido - Upload d'un passeport tronqué web - NP (SOUS-2552)
    And l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce tronquée
    Then un message d'erreur "Image tronquée" apparait
    And le bouton "Recommencer" est présent

  @CNP_ID_flou
  Scenario: SDK Onfido - SDK Onfido - Upload d'une carte d'identité Floue web - NP (SOUS-2627)
    And l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce floue
    Then un message d'erreur "Image floue" apparait
    And le bouton "Recommencer" est présent

  @CNP_ID_reflet
  Scenario: SDK Onfido - Upload d'un titre de séjour recto reflet web - NP (SOUS-2556)
    And l'utilisateur clique sur "Envoyer une photo" et sélectionne puis télécharge sa pièce avec reflet
    Then un message d'erreur "Image avec reflet" apparait
    And le bouton "Recommencer" est présent
