Feature: Vérification des boutons sur la page private-life-certification

  Background: 
    Given la page private-life-certification s'affiche

  @ValidPrivateLifeCertif
  Scenario: l'utilisateur valide la page private-life-certification
    When l'utilisateur clique sur certification
    And l'utilisateur clique sur suivant _Citoyen US
    Then la page account-type s'affiche

  @Update_RF_PrivateLifeCertif
  Scenario: Vérification du bouton "Modifier"_Résidence fiscale_Page private-life-certification
    When l'utilisateur clique sur modifier_RF
    Then la page tax_residence s'affiche

  @Update_CitoyenUS_PrivateLifeCertif
  Scenario: Vérification du bouton "Modifier"_CitoyenUS_Page private-life-certification
    When l'utilisateur clique sur modifier_CitoyenUS
    Then l'utilisateur est sur la page Citoyen US
