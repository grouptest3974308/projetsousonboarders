import java.nio.file.Path
import java.nio.file.Paths

import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable



class Config {
	/**
	 * Executes before every test case starts.
	 * @param testCaseContext related information of the executed test case.
	 */
	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
		println testCaseContext.getTestCaseId()
		println testCaseContext.getTestCaseVariables()
		String locale=GlobalVariable.locale
		Map prefs = [('intl.accept_languages'):locale,('profile.default_content_setting_values.media_stream_mic'):1,
			('profile.default_content_setting_values.media_stream_mic'): 1,
			('profile.default_content_setting_values.media_stream_camera'):  1,
			('profile.default_content_setting_values.geolocation'): 1,
			('profile.default_content_setting_values.notifications'): 1]

		RunConfiguration.setWebDriverPreferencesProperty("prefs",prefs)
		
		DesiredCapabilities capabilities = new DesiredCapabilities()
				
		String projectDir = RunConfiguration.getProjectDir()
		println("projectDir +++++++++++++++++++ "+ projectDir);
		projectDir=projectDir.replaceAll("C:/", "")
		println("projectDir 2 +++++++++++++++++++ "+ projectDir);

		Path projectPath = Paths.get(projectDir)
		println("projectPath +++++++++++++++++++ "+ projectPath)

		def customizedDownloadDirectory = projectPath.toString()
		println("customizedDownloadDirectory +++++++++++++++++++ "+ customizedDownloadDirectory)
		//Path downloadPath = projectPath.resolve(GlobalVariable.URLfakePhoto)
		//def customizedDownloadDirectory = downloadPath.toString()

		String argum =WebUI.concatenate(["--use-file-for-fake-video-capture=/", projectDir, "/Images/selfie.mjpeg"] as String[], FailureHandling.STOP_ON_FAILURE)
		println("argum +++++++++++++++++++ "+ argum)

		RunConfiguration.setWebDriverPreferencesProperty("args", ["--start-maximized", argum, "--use-fake-device-for-media-stream"])
	
	}
}