import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.And as And
import cucumber.api.java.en.Then as Then
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//Added on 11/10/2021
import com.kms.katalon.core.annotation.Keyword as Keyword
//Added on 20/09/2021
import groovy.json.JsonSlurper as JsonSlurper
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberReporter as CucumberReporter
import com.kms.katalon.core.cucumber.keyword.CucumberRunnerResult as CucumberRunnerResult
import com.kms.katalon.core.cucumber.keyword.internal.CucumberRunnerResultImpl as CucumberRunnerResultImpl
import com.kms.katalon.core.keyword.BuiltinKeywords as BuiltinKeywords
import com.kms.katalon.core.keyword.internal.KeywordMain as KeywordMain
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.model.RunningMode as RunningMode
import cucumber.api.cli.Main as Main
import groovy.transform.CompileStatic as CompileStatic
import com.kms.katalon.core.util.internal.PathUtil as PathUtil

CucumberKW.runFeatureFileWithTags('Include/features/SOUS_01_Ajout adresse e-mail.feature', ((['@validEmail']) as String[]))

CucumberKW.runFeatureFileWithTags('Include/features/SOUS_02_SDK_onfido.feature', ((['@validID']) as String[]))

CucumberKW.runFeatureFileWithTags('Include/features/SOUS_03_a_Selfie.feature', ((['@Selfie']) as String[]))

CucumberKW.runFeatureFileWithTags('Include/features/SOUS_03_b_Consentement_biométrique.feature', ((['@AcceptConsent']) as String[]))

CustomKeywords.'utilities.KW0_SetSituationFamiliale.setSituationFamiliale'(situation_familiale.toString())

CustomKeywords.'utilities.KW1_ClickSituationPatrimo.setSituationPatri'(situation_patrimoniale.toString(), prenom_heb.toString(), 
    nom_heb.toString(), dateNaissance_heb.toString())

CucumberKW.runFeatureFileWithTags('Include/features/SOUS_04_Coordonnées.feature', ((['@AddManAdd']) as String[]))

CucumberKW.runFeatureFileWithTags('Include/features/SOUS_05_ResidenceFiscale.feature', ((['@RésidenceFiscaleUniqueFr']) as String[]))

CucumberKW.runFeatureFileWithTags('Include/features/SOUS_06_Citoyen_US.feature', ((['@NonCitoyenUS']) as String[]))

CucumberKW.runFeatureFileWithTags('Include/features/SOUS_07_PrivateLifeCertif.feature', ((['@ValidPrivateLifeCertif']) as String[]))

CustomKeywords.'utilities.KW3_ClickTypeCompte.clickTypeCompte'(type_cmp.toString())

CustomKeywords.'utilities.KW4_ClickUsageCompte.clickUsageCompte'([findTestData('KY3_Data').getValue(7, 1), findTestData(
            'KY3_Data').getValue(7, 2), findTestData('KY3_Data').getValue(7, 3), '', '', '', ''])

CustomKeywords.'utilities.KW5_ClickStatut.clickStatut'(statut.toString())

CustomKeywords.'utilities.KW6_1_ClickAncienneSituation.clickAncienneSituation1'(ancienne_situation.toString())

CustomKeywords.'utilities.KW6_ter_UsageCompte.selectUsage'(usage_cmp.toString())

CustomKeywords.'utilities.KW7_ClickRevenus.clickRevenu'(revenus.toString())

CustomKeywords.'utilities.KW8_ClickPatrimoine.clickPatrimoine'(patrimoine.toString())

CucumberKW.runFeatureFileWithTags('Include/features/SOUS_08_Civility_confirmation.feature', ((['@ValidCivilityInformation']) as String[]))

not_run: CucumberKW.runFeatureFileWithTags('Include/features/SOUS_09_Signature_CDG.feature', ((['@VerifCDG_NePasAccepter']) as String[]))

not_run: CucumberKW.runFeatureFileWithTags('Include/features/SOUS_10_Ajout numéro de téléphone.feature', ((['@ValidNum']) as String[]))

not_run: CucumberKW.runFeatureFile('Include/features/SOUS_11_Challenge_SMS.feature')

WebUI.closeBrowser()

