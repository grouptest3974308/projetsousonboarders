import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys

//Added 17/09/2021
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import groovy.json.JsonSlurper as JsonSlurper

GlobalVariable.firstName="ALIZÉ JEANNE"
GlobalVariable.lastName="MOUCHART"
println ("+++++++++++++++++++++ First name (for Challenge SMS):"+ GlobalVariable.firstName.toUpperCase() + "++++++++++++++++++++++++++++")
println ("+++++++++++++++++++++ Last name  (for Challenge SMS) :"+ GlobalVariable.lastName.toUpperCase()+ "++++++++++++++++++++++++++++")

def responseAPI = WS.sendRequest(findTestObject('Object Repository/Challenge_SMS'))
WS.verifyResponseStatusCode(responseAPI, 200)
def slurper = new JsonSlurper()
def resultAPI = slurper.parseText(responseAPI.getResponseBodyContent())
def String challengeSMS
int index = 0
int nbFound = 0

while (index < resultAPI.size && resultAPI[index].lastName != null && resultAPI[index].firstName != null) {
	
	println("++++++++++++++++++  index  = "+index)

	String lastNameAPIstr  = resultAPI[index].lastName.toUpperCase()
	String firstNameAPIstr = resultAPI[index].firstName.toUpperCase()
	lastNameAPIstr= lastNameAPIstr.replace("_"," ")
	firstNameAPIstr= firstNameAPIstr.replace("_"," ")
	
	println("resultat 1 ="+ lastNameAPIstr.trim())
	println("resultat 2 ="+ GlobalVariable.lastName.toUpperCase())
	println("resultat 3 ="+ firstNameAPIstr.trim()  )
	println("resultat 4 ="+ GlobalVariable.firstName.toUpperCase())
	
	
	if(lastNameAPIstr.trim() == GlobalVariable.lastName.toUpperCase()) {
		if(firstNameAPIstr.trim() == GlobalVariable.firstName.toUpperCase()){
		println("Index *********************"+ index  )
		println("SMS *********************"+  resultAPI[index].sms3  )
		challengeSMS = resultAPI[index].sms3
		println(" ********************* ChallengeSMS: "+  challengeSMS )
		index = resultAPI.size
		nbFound += 1
	}}
	index+=1
}

if(nbFound == 0){
	KeywordUtil.markError('''************************* SMS Challenge non retrouvé *************************''')
		}
//println("challengeSMS 2 *********************" +  challengeSMS  )