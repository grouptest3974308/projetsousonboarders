import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//Added on 20/09/2021
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import java.text.MessageFormat as MessageFormat
import org.apache.commons.lang3.StringUtils as StringUtils
import org.junit.runner.Computer as Computer
import org.junit.runner.JUnitCore as JUnitCore
import org.junit.runner.Result as Result
import org.junit.runner.notification.Failure as Failure
import groovy.json.JsonSlurper as JsonSlurper
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberReporter as CucumberReporter
import com.kms.katalon.core.cucumber.keyword.CucumberRunnerResult as CucumberRunnerResult
import com.kms.katalon.core.cucumber.keyword.internal.CucumberRunnerResultImpl as CucumberRunnerResultImpl
import com.kms.katalon.core.keyword.BuiltinKeywords as BuiltinKeywords
import com.kms.katalon.core.keyword.internal.KeywordMain as KeywordMain
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.model.RunningMode as RunningMode
import cucumber.api.cli.Main as Main
import groovy.transform.CompileStatic as CompileStatic
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
	



import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys

//Added 17/09/2021
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import groovy.json.JsonSlurper as JsonSlurper


def responseAPI = WS.sendRequest(findTestObject('Object Repository/Challenge_SMS'))
WS.verifyResponseStatusCode(responseAPI, 200)
def slurper = new JsonSlurper()
def resultAPI = slurper.parseText(responseAPI.getResponseBodyContent())
def String challengeSMS
int index = 0
int nbFound = 0
println(GlobalVariable.lastName )
println(GlobalVariable.firstName )


while (index < resultAPI.size ) {
	if (  resultAPI[index].lastName != null && resultAPI[index].firstName != null )
	{
		println("Je suis rentré dans le premier if "+ index  )
		println("Data Result "+ resultAPI[index].lastName.toUpperCase()  )
		println("Data Variable "+ GlobalVariable.lastName.toUpperCase()  )
		if (resultAPI[index].lastName.toUpperCase()  == GlobalVariable.lastName.toUpperCase() )
		
			{		println("Je suis rentré dans le second if "+ index  )
				
				if (resultAPI[index].firstName.toUpperCase() == GlobalVariable.firstName.toUpperCase())
				{
				
				
				
				println("Index *********************"+ index  )
				println("SMS *********************"+  resultAPI[index].sms3  )
				challengeSMS = resultAPI[index].sms3
				println(" ********************* ChallengeSMS: "+  challengeSMS )
				index = resultAPI.size
				nbFound += 1
				}
			}
	}
	index+=1

}



if(nbFound == 0){
	KeywordUtil.markError('''************************* SMS Challenge non retrouvé *************************''')
}



