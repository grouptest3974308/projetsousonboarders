import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.hssf.record.PageBreakRecord.Break as Break
import org.openqa.selenium.Keys as Keys
//Added on 20/09/2021
import groovy.json.JsonSlurper as JsonSlurper
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberReporter as CucumberReporter
import com.kms.katalon.core.cucumber.keyword.CucumberRunnerResult as CucumberRunnerResult
import com.kms.katalon.core.cucumber.keyword.internal.CucumberRunnerResultImpl as CucumberRunnerResultImpl
import com.kms.katalon.core.keyword.BuiltinKeywords as BuiltinKeywords
import com.kms.katalon.core.keyword.internal.KeywordMain as KeywordMain
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.model.RunningMode as RunningMode
import cucumber.api.cli.Main as Main
import groovy.transform.CompileStatic as CompileStatic
import com.kms.katalon.core.util.internal.PathUtil as PathUtil

responseAPI = WS.sendRequest(findTestObject('Object Repository/Challenge_SMS'))

WS.verifyResponseStatusCode(responseAPI, 200)

def slurper = new JsonSlurper()

def resultAPI = slurper.parseText(responseAPI.getResponseBodyContent())

println('resultAPI[i].firstName******************** ' + resultAPI[0].firstName)

println('GlobalVariable.firstName ******************' + GlobalVariable.firstName)

println('resultAPI.size******************************' + resultAPI.size)

String challengeSMS

int index = 0

while ((index < resultAPI.size) & (resultAPI[index].lastName.toUpperCase() != GlobalVariable.lastName.toUpperCase())) {
    index += 1
}

if (resultAPI[index].firstName.toUpperCase() == GlobalVariable.firstName.toUpperCase()) {
    println('index *********************' + index)

    println('SMS *********************' + resultAPI[index].sms3)

    challengeSMS = resultAPI[index].sms3

    println('challengeSMS *********************' + challengeSMS)
} else {
    System.out.println('************************* SMS Challenge non retrouvé *************************')
}

println('challengeSMS *********************' + challengeSMS)

WebUI.acceptAlert()

