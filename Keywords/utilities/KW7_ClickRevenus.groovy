package utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
//import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class KW7_ClickRevenus {
	@Keyword
	def clickRevenu(String revenus) {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/page_financial'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/000_button_revenus'),4)
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/000_button_revenus'),4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/000_button_revenus'))

		switch (revenus) {
			case "- de 500€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/00_li_- de 500'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/00_li_- de 500'))
				break
			case "entre 500€ et 1 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/01_li_entre 500 et 1 000'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/01_li_entre 500 et 1 000'))
				break
			case "entre 1 001€ et 1 500€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/02_li_entre 1 001 et 1 500'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/02_li_entre 1 001 et 1 500'))
				break
			case "entre 1 501€ et 2 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/03_li_entre 1 501 et 2 000'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/03_li_entre 1 501 et 2 000'))
				break
			case "entre 2 001€ et 3 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/04_li_entre 2 001 et 3 000'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/04_li_entre 2 001 et 3 000'))
				break
			case "+ de 3 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/05_li_de 3 000'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/1_Revenus/05_li_de 3 000'))
				break
		}
	}
}