package utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class KW3_ClickTypeCompte {
	@Keyword
	def clickTypeCompte(String type_cmp) {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/1_BankAccountType/page_account-type'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/1_BankAccountType/button_Cmp_principal'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/1_BankAccountType/button_Cmp_secondaire'), 4)
		if (type_cmp == "Principal") {
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/1_BankAccountType/button_Cmp_principal'), 4)
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/1_BankAccountType/button_Cmp_principal'))
			Thread.sleep(2000)
		} else if (type_cmp == "Secondaire"){
			WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/1_BankAccountType/button_Cmp_secondaire'), 4)
			WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/1_BankAccountType/button_Cmp_secondaire'))
			Thread.sleep(2000)
		}
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/page_account-use'), 4)
	}
}
