package utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
//import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

/*public class KW6_2_ClickAncienneSituation {
	@Keyword
	def clickAncienneSituation2(String ancienne_situation) {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/000_button_ancienne_sit'), 4)
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/000_button_ancienne_sit'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/000_button_ancienne_sit'))
		switch (ancienne_situation) {
			case "Artisan(e), commerçant(e), chef d'entreprise":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/00_li_Artisan(e), commerant(e), chef dentreprise'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/00_li_Artisan(e), commerant(e), chef dentreprise'))
				break
			case "Agriculteur(rice) exploitant(e)":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/01_li_Agriculteur(rice) exploitant(e)'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/01_li_Agriculteur(rice) exploitant(e)'))
				break
			case "Employé(e) et ouvrier(e)":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/02_li_Employ(e) et ouvrier(e)'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/02_li_Employ(e) et ouvrier(e)'))
				break
			case "Cadre et profession intermédiaire":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/03_li_Cadre et profession intermdiaire'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/03_li_Cadre et profession intermdiaire'))
				break
			case "Travailleur(se) indépendant(e) et profession libérale":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/04_li_Travailleur(se) indpendant(e) et profession librale'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_2_Ancienne_situation/04_li_Travailleur(se) indpendant(e) et profession librale'))
				break
		}
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/page_financial'), 4)
	}
}*/
