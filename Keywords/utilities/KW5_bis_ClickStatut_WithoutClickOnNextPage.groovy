package utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class KW5_bis_ClickStatut_WithoutClickOnNextPage {
	@Keyword
	def clickStatut(String statut, String usage_cmp) {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/page_employment'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/000_button_statut'), 4)
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/000_button_statut'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/000_button_statut'))

		switch (statut) {
			case "Employé(e)":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/00_Employé(e)'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/00_Employé(e)'))
				break
			case "Auto-entrepreneur(se)":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/01_Auto-entrepreneur(se)'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/01_Auto-entrepreneur(se)'))
				break
			case "Entrepreneur(se)":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/02_Entrepreneur(se)'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/02_Entrepreneur(se)'))
				break
			case "Retraité(e)":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/03_Retraité(e)'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/03_Retraité(e)'))
				break
			case "Demandeur(se) d'emploi":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/04_Demandeur(se) demploi'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/04_Demandeur(se) demploi'))
				break
			case "Profession libérale":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/05_Profession libérale'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/05_Profession libérale'))
				break
			case "Artisan(e)":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/06_Artisan(e)'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/06_Artisan(e)'))
				break
			case "Sans revenus réguliers":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/07_Sans revenus réguliers'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/07_Sans revenus réguliers'))
				break
			case "Commerçant(e)":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/08_Commerçant(e)'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/08_Commerçant(e)'))
				break
			case "Fonctionnaire":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/09_Fonctionnaire'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/09_Fonctionnaire'))
				break
			case "Cadre":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/10_Cadre'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/10_Cadre'))
				break
			case "Etudiant(e)":
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/11_Etudiant(e)'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/11_Etudiant(e)'))
				break
		}
	}
}
