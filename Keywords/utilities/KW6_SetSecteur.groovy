package utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
//import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class KW6_SetSecteur {
	@Keyword
	def clickSecteur(String secteur) {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_1_Secteur/input_secteur'), 4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_1_Secteur/input_secteur'), secteur)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/page_employment'))
		if (secteur == '''Artisanat et métier d'art''') {
			checkSubsector()
		}else if (secteur == '''Audit, compatibilité, gestion''') {
			checkSubsector()
		}else if (secteur == '''Automobile''') {
			checkSubsector()
		}else if (secteur == '''Banque, assurance''') {
			checkSubsector()
		}else if (secteur == '''Commerce - distribution''') {
			checkSubsector()
		}else if (secteur == '''Eau & Environnement''') {
			checkSubsector()
		}else if (secteur == '''Energie''') {
			checkSubsector()
		}else if (secteur == '''Immobilier''') {
			checkSubsector()
		}else if (secteur == '''Spectacle - activités récréatives - création''') {
			checkSubsector()
		}else if (secteur == '''Sport''') {
			checkSubsector()
		}else if (secteur == '''Transport - Logistique''') {
			checkSubsector()
		}
		//		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		//		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/page_financial'), 4)
	}
	def checkSubsector() {
		String subsectorValue = WebUI.executeJavaScript("return document.getElementById('subSector').value ", null)
		println("++++++++++++++++++++++++++++++++ SubsectorValue before check  " + subsectorValue + "   ++++++++++++++++++++++++++++++++")
		if(subsectorValue != null) {
			if(subsectorValue != ""){
				KeywordUtil.markError('''************************* Erreur: La checkbox est intitialement cochée *************************''')
			}
		}

		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_1_Secteur/input_subSector'), 4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_1_Secteur/input_subSector'))

		String SubsectorValue  = WebUI.executeJavaScript("return document.getElementById('subSector').value ", null)
		println("++++++++++++++++++++++++++++++++ SubsectorValue after check "+ SubsectorValue +"   ++++++++++++++++++++++++++++++++")
		if (subsectorValue == null){
			KeywordUtil.markError('''************************* Erreur: La checkbox n'a pas été cochée 1*************************''')
		}
		if (subsectorValue == " "){
			KeywordUtil.markError('''************************* Erreur: La checkbox n'a pas été cochée 2*************************''')
		}

	}
}