package utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
//import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class KW1_ClickSituationPatrimo {
	@Keyword
	def setSituationPatri(String situation_patrimoniale, prenom_heb, nom_heb, dateNaissance_heb) {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/page_property_status'), 4)
		switch (situation_patrimoniale) {
			case "Propriétaire":
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/1_Propritaire'))
				break
			case "Locataire":
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/0_Locataire'))
				break
			case "Hébergé":
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/2_Hberg'))
				ajoutDonneesHebergeur(prenom_heb, nom_heb, dateNaissance_heb)
				break
		}

		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/02_Coordonnées/Page_Adresse/page_adresse'), 4)

	}
	def ajoutDonneesHebergeur(String prenom_heb,String nom_heb, String dateNaissance_heb ) {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/input_heb_lastName'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/input_heb_firstName'),4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/input_heb_birthDate'),4)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/input_heb_lastName'), nom_heb )
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/input_heb_firstName'), prenom_heb)
		WebUI.setText(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/input_heb_birthDate'), dateNaissance_heb )
	}

}


