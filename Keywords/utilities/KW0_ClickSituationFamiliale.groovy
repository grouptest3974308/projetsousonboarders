package utilities

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class KW0_SetSituationFamiliale {
	@Keyword
	def setSituationFamiliale(String situation_familiale) {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/00_Situation_Familiale/page_family_status'), 4)
		switch (situation_familiale) {
			case "Célibataire":
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/00_Situation_Familiale/1_Celib'))
				break
			case "Marié/Pacsé":
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/00_Situation_Familiale/2_Mar_Pacsé'))
				break
			case "En concubinage":
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/00_Situation_Familiale/3_Concub'))
				break
			case "Divorcé":
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/00_Situation_Familiale/4_Divorce'))
				break
			case "Veuf":
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/00_Situation_Familiale/5_Veuf'))
				break
		}
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/1_PersonalData/01_Situation_Patrimo/page_property_status'), 4)
	}
}
