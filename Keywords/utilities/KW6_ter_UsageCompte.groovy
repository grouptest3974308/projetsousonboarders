package utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class KW6_ter_UsageCompte {
	@Keyword
	def selectUsage(String usage_cmp) {
		if(WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/usage_priv'), 4,FailureHandling.OPTIONAL )) {

			if (usage_cmp == "Privé") {
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/usage_priv'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/usage_priv'))
				Thread.sleep(2000)
			} else if (usage_cmp == "Professionnel") {
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/usage_pro'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/usage_pro'))
				Thread.sleep(2000)
				WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/popup_Nickel pour les entreprise'), 4)
				WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/popup_Nickel pour les entreprise'), 4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/button_Ouvrir_cmp_usage priv'))
				Thread.sleep(2000)
				WebUI.waitForElementNotPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/popup_Nickel pour les entreprise'), 4)
				//attribute_val= WebUI.getAttribute(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/usage_pro'), 'aria-checked')
				//println(attribute_val)
				//WebUI.verifyElementChecked(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/01_3_Usage/usage_priv'),4)
			}
		}
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/page_financial'), 4)
	}
}