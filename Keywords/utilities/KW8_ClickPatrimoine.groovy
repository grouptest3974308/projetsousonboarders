package utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
//import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//Added 17/08/2021
import com.kms.katalon.core.configuration.RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths
//Added 18/08/2021
import org.openqa.selenium.Keys as Keys

public class KW8_ClickPatrimoine {
	@Keyword
	def clickPatrimoine(String patrimoine) {
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/000_button_patrimoine'),4)
		WebUI.waitForElementClickable(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/000_button_patrimoine'),4)
		WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/000_button_patrimoine'))

		switch (patrimoine) {
			case "- de 20 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_- de 20 000'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_- de 20 000'))
				break
			case "entre 20 000€ et 100 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_entre 20 000 et 100 000'),4)
				WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/input_patrimoine'), Keys.chord(Keys.DOWN))
				WebUI.sendKeys(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/input_patrimoine'), Keys.chord(Keys.ENTER))
				break
			case "entre 100 001€ et 250 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_entre 100 001 et 250 000'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_entre 100 001 et 250 000'))
				break
			case "entre 250 001€ et 500 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_entre 250 001 et 500 000'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_entre 250 001 et 500 000'))
				break
			case "entre 500 001€ et 1 000 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_entre 500 001 et 1 000 000'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_entre 500 001 et 1 000 000'))
				break
			case "+ de 1 000 000€":
				WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_de 1 000 000'),4)
				WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/4_Financial/2_Patrimoine/li_de 1 000 000'))
				break
		}
		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/3_Civility-confirmation/page_civility-confirmation'),4)
	}
}
