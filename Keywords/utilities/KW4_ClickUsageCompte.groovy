package utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class KW4_ClickUsageCompte {
	@Keyword
	def clickUsageCompte(List use_of_cmp) {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/page_account-use'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/01_Recevoir salaire, retraite etc'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/02_Payer mes charges courantes'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/03_Régler des dpenses saisonnire etc'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/04_Encaisser mon chiffre daffaires'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/05_Effectuer des achats en ligne'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/06_Payer a letranger'), 4)
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/07_Autres utilisations'), 4)
		//Suppression de éléments vide de la liste
		use_of_cmp.removeAll("")
		//Indice du dernier élément de la liste
		def last_item = use_of_cmp.size-1
		for (def index:0..last_item) {
			switch (use_of_cmp[index] ) {
				case "Recevoir mes salaires, retraite, CAF, Pôle emploi, etc.":
					WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/01_Recevoir salaire, retraite etc'), 4)
					WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/01_Recevoir salaire, retraite etc'))
					break
				case "Recevoir mes salaires, retraite, CAF, Pôle emploi, etc.":
					WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/01_Recevoir salaire, retraite etc'), 4)
					WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/01_Recevoir salaire, retraite etc'))
					break
				case "Payer mes charges courantes : eau, électricité, gaz, téléphonie etc.":
					WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/02_Payer mes charges courantes'), 4)
					WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/02_Payer mes charges courantes'))
					break
				case "Régler des dépenses saisonnières, vacances, pot commun, etc.":
					WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/03_Régler des dpenses saisonnire etc'), 4)
					WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/03_Régler des dpenses saisonnire etc'))
					break
				case "Encaisser mon chiffre d’affaires d’auto-entrepreneur.":
					WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/04_Encaisser mon chiffre daffaires'), 4)
					WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/04_Encaisser mon chiffre daffaires'))
					Thread.sleep(4000)
					WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/Popup/div_Nickel pas pour les entreprises'), 4)
					WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/Popup/div_Nickel pas pour les entreprises'), 4)
					WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/Popup/button_Ouvrir un compte Nickel usage priv'))
					WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/04_Encaisser mon chiffre daffaires'), 4)
					break
				case "Effectuer des achats en ligne.":
					WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/05_Effectuer des achats en ligne'), 4)
					WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/05_Effectuer des achats en ligne'))
					break
				case "Payer à l'étranger.":
					WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/06_Payer a letranger'), 4)
					WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/06_Payer a letranger'))
					break
				case "Autres utilisations.":
					WebUI.verifyElementPresent(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/07_Autres utilisations'), 4)
					WebUI.click(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/2_Usage/07_Autres utilisations'))
					break
			}
		}

		WebUI.click(findTestObject('Object Repository/SpyElements/button_Suivant'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/SpyElements/2_Add_Data/2_AcountData/3_Employment/00_Statut/page_employment'), 4)
	}
}


