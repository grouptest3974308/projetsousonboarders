<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Profil - Resident Card</description>
   <name>TS_02_a</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>a97f2f64-3d10-4c85-bfd8-4b62171161fb</testSuiteGuid>
   <testCaseLink>
      <guid>cb762c94-8f7e-4709-95f7-0d1749c97843</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02_Upload_ID/TC_17_Upload_ResidentCard_Reflet_CNP</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e3911d05-41f1-4a83-9e7a-9b92414bd103</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02_Upload_ID/TC_13_Upload_Valid_ResidentCard</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
