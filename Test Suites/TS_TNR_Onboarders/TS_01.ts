<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Default Profile - ID Card</description>
   <name>TS_01</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>585fd071-6db6-4a4b-be1b-1cfe196b08b7</testSuiteGuid>
   <testCaseLink>
      <guid>9e45beff-76c7-4cbe-8bb8-eb76d3f42bf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00_Code_Parrainage/TC_01_ValidReferralCode</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d62b9d3d-546c-4eb7-85c5-ce8d968c5c05</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0429b55a-a6c6-45bd-bfff-ce6729e35366</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00_Code_Parrainage/TC_02_InvalidReferralCode_1</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d154e6b7-85c3-4ae6-8336-c90ba1657e10</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9bdf5ab3-6948-44fa-aea9-ebd779d5e6bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00_Code_Parrainage/TC_03_InvalidReferralCode_2</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>50c33171-c6d9-4689-b441-011c7351ae13</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3928e02b-e032-466b-85cc-8793e3a8cc3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00_Code_Parrainage/TC_04_InvalidReferralCode_3</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4259e4a7-6c8a-4202-964a-eb5c123df583</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2e1a57ba-0b62-40c2-a503-ea9b84541934</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Email/TC_05_ValidEmail</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a4fde261-c2bf-4394-98de-bc90074a8b2f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>613b5539-7128-4b2e-bfb6-d22a0d5a20b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Email/TC_06_InvalidEmail_1</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3bde21d7-1b02-4f39-a070-51c51fec52aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Email/TC_07_InvalidEmail_2</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a11f1ed2-6f8c-4777-815e-7311390b484d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Email/TC_08_InvalidEmail_3</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1512137c-15cd-4a6d-85eb-22a6721e016f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Email/TC_09_WithoutEmail</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8b65db79-6071-4562-be67-1a9f76c3c7ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/01_Email/TC_10_TimeToDisplayInvalidEmail_ToDo</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5ed0a277-acfd-4aa5-a5a2-d1122931ecbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02_Upload_ID/TC_12_Upload_Valid_ID</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>94fea146-8169-4507-a994-86f7029fc046</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02_Upload_ID/TC_16_Upload_ID_Flou_CNP</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>88575d55-21e1-49b4-b88c-f6bd6d20ebce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03_Selfie/TC_21_Customer_Restart_Selfie</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d0b065fb-090b-4628-9456-606a9fbbc1d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/03_Selfie/TC_20_Selfie_CloseConsent_ToBeUpdated</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c1fed47d-d3a0-4d7b-be41-ab29ee8a08e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03_Selfie/TC_19_Selfie_DenyConsent</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9e93763a-2079-4ce8-a56b-862b4d217613</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03_Selfie/TC_18_Selfie_AcceptConsent</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
